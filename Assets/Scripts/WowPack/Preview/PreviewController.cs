﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class PreviewController : MonoBehaviour
{
    public WorldImageTracking WorldImageTracking;
    public GameObject MainPanel;
    public GameObject PreviewPanel;
    public GameObject PreviewVideoPanel;
    public RectTransform PreviewPhotoPanel;
    public RectTransform VideoPanel;
    //public RectTransform PhotoPanel;
    //public RectTransform VideoPanel;
    public UnityEngine.UI.Image PhotoPreview;
    //public VideoPlayer VideoPlayerPreview;
    public PreviewVideoPlayer PreviewVideoPlayer;

    public bool IsManualRectSize = false;
    public float InitRectWidth = 1515f; //1515
    public float InitRectHeight = 800f; //800

    private float m_initWidth;
    private float m_initHeight;
    private Texture2D m_previewTexture;
    private string m_previewPathVideo;

    string subject = "WoW Pack";
    string text = "";

    public enum PreviewState
    {
        NONE,
        IMAGE,
        VIDEO
    }

    private PreviewState previewState = PreviewState.NONE;

    private void Awake()
    {
        if (IsManualRectSize)
        {
            m_initWidth = InitRectWidth;
            m_initHeight = InitRectHeight;
        }
        else
        {
            InitRectWidth = PreviewPhotoPanel.sizeDelta.x;
            InitRectHeight = PreviewPhotoPanel.sizeDelta.y;

            m_initHeight = InitRectHeight;
            m_initWidth = InitRectWidth;
        }

        OnScreenTransformDimensionsChange();
    }

    public void Start()
    {
        //VideoPlayerPreview.prepareCompleted += PrepareVideoPlayer;
        PreviewVideoPlayer.videoPlayer.prepareCompleted += PrepareVideoPlayer;
    }

    protected void OnRectTransformDimensionsChange()
    {
        OnScreenTransformDimensionsChange();
    }

    private void OnScreenTransformDimensionsChange()
    {
        if (Screen.width > Screen.height)
        {
            m_initWidth = InitRectHeight;
            m_initHeight = InitRectWidth;
        }
        else
        {
            m_initHeight = InitRectHeight;
            m_initWidth = InitRectWidth;
        }

        if (previewState == PreviewState.IMAGE)
        {
            UpdatePreviewImageWithAspectRatio(m_previewTexture);
        }

        if (previewState == PreviewState.VIDEO)
        {
            UpdatePreviewVideo(m_previewPathVideo);
            //UpdatePreviewVideo(VideoPlayerPreview.url);
        }
    }

    public void Share()
    {
        if (previewState == PreviewState.IMAGE)
        {
            new NativeShare().AddFile(m_previewTexture)
                .SetSubject(subject).SetText(text)
                .SetCallback((result, shareTarget) =>
                {
                    if (result == NativeShare.ShareResult.Shared)
                    {
                        Close();
                    }
                    else
                    {
                        //TODO
                    }
                })
                .Share();
            return;
        }

        if (previewState == PreviewState.VIDEO)
        {
            new NativeShare().AddFile(m_previewPathVideo)
            .SetSubject(subject).SetText(text)
            .SetCallback((result, shareTarget) =>
            {
                if (result == NativeShare.ShareResult.Shared)
                {
                    Close();
                }
                else
                {
                    //TODO
                }
            })
            .Share();
            return;
        }
    }

    public void Close()
    {
        WorldImageTracking.Tracking(true);
        PreviewVideoPanel.gameObject.SetActive(false);
        PreviewPhotoPanel.gameObject.SetActive(false);
        PreviewPanel.SetActive(false);
        MainPanel.SetActive(true);

        if (previewState == PreviewState.IMAGE)
        {

        }

        if (previewState == PreviewState.VIDEO)
        {
            PreviewVideoPlayer.Stop();
            File.Delete(m_previewPathVideo);
        }

        previewState = PreviewState.NONE;
    }

    public void UpdatePreviewImage(Texture2D texture)
    {
        WorldImageTracking.Tracking(false);

        previewState = PreviewState.IMAGE;
        MainPanel.SetActive(false);
        PreviewPanel.SetActive(true);
        PreviewVideoPanel.gameObject.SetActive(false);
        PreviewPhotoPanel.gameObject.SetActive(true);
        m_previewTexture = texture;
        PhotoPreview.sprite = Sprite.Create(texture, new Rect(0.0f, 0.0f, texture.width, texture.height),
                   new Vector2(0.5f, 0.5f), 100.0f);
    }

    public void UpdatePreviewImageWithAspectRatio(Texture2D texture)
    {
        UpdatePreviewImage(texture);
        float width = m_initWidth;
        float height = m_initHeight;
        float k = texture.height / texture.width;

        if (k > 0)
        {
            PreviewPhotoPanel.sizeDelta = new Vector2(height * (float)texture.width / (float)texture.height, height);
        }
        else
        {
            PreviewPhotoPanel.sizeDelta = new Vector2(width, (float)texture.height / (float)texture.width * width);
        }
    }

    public void UpdatePreviewVideo(string path)
    {
        WorldImageTracking.Tracking(false);
        previewState = PreviewState.VIDEO;

        MainPanel.SetActive(false);
        PreviewPanel.SetActive(true);
        PreviewPhotoPanel.gameObject.SetActive(false);
        PreviewVideoPanel.gameObject.SetActive(true);

        m_previewPathVideo = path;
        //VideoPlayerPreview.Stop();
        PreviewVideoPlayer.Prepare(path);
        //VideoPlayerPreview.clip = VideoPlayerPreview.clip;
    }

    private void PrepareVideoPlayer(VideoPlayer source)
    {
        float width = m_initWidth;
        float height = m_initHeight;
        float k = source.height / source.width;

        if (k > 0)
        {
            VideoPanel.sizeDelta = new Vector2(height * (float)source.width / (float)source.height, height);
        }
        else
        {
            VideoPanel.sizeDelta = new Vector2(width, (float)source.height / (float)source.width * width);
        }

        PreviewVideoPanel.GetComponent<RectTransform>().sizeDelta = new Vector2(width, height);
        //VideoPlayerPreview.Play();
    }
}
