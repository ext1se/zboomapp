﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using YoutubeLight;
using SimpleJSON;
using System.Text;
using System;
using UnityEngine.Events;
using UnityEngine.UI;
using TMPro;
using UnityEngine.EventSystems;

public class PreviewVideoPlayer : MonoBehaviour
{
    [Space]
    [Header("The unity video players")]
    [Tooltip("The unity video player")]
    public VideoPlayer videoPlayer;

    [Tooltip("Hide the controls if use not interact in desired time, 0 equals to not hide")]
    public int autoHideControlsTime = 0;

    public bool simpleTouchCheck = false;

    [Tooltip("The main controller ui parent")]
    public GameObject mainControllerUi;

    [Tooltip("Play button")]
    public Image playButton;

    [Tooltip("Pause button")]
    public Image pauseButton;

    [Tooltip("Slider with duration and progress")]
    public Slider progress;

    [Tooltip("Volume slider")]
    public Slider volumeSlider;

    [Tooltip("Playback speed")]
    public Slider playbackSpeed;

    [Tooltip("Full Time")]
    public TextMeshProUGUI timeInfoText;

    [HideInInspector]
    public bool pauseCalled = false;

    private bool progressEnabled = true;
    private Coroutine coroutine;

    private float totalVideoDuration;
    private float currentVideoDuration;
    private float hideScreenTime = 0;
    private bool showingPlaybackSpeed = false;
    private bool showingVolume = false;

    private Camera mainCamera;

    public void Awake()
    {

    }

    public void Start()
    {
        mainCamera = Camera.main;
    }

    private void OnApplicationPause(bool pause)
    {
        if (videoPlayer.isPrepared)
        {
            videoPlayer.Pause();
        }
    }

    private void OnApplicationFocus(bool focus)
    {
        if (focus == true)
        {
            if (!pauseCalled)
            {
                if (videoPlayer.isPrepared)
                {
                    videoPlayer.Play();
                }
            }
        }
    }

    void FixedUpdate()
    {
        if (videoPlayer.isPlaying)
        {
            totalVideoDuration = Mathf.RoundToInt(videoPlayer.frameCount / videoPlayer.frameRate);
            currentVideoDuration = Mathf.RoundToInt(videoPlayer.frame / videoPlayer.frameRate);
        }

        if (videoPlayer.frameCount > 0)
        {
            if (progress != null)
            {
                if (progressEnabled)
                {
                    progress.SetValueWithoutNotify((float)videoPlayer.frame / (float)videoPlayer.frameCount);
                }
            }
        }

        if (timeInfoText != null)
        {
            string currentTime = FormatTime(Mathf.RoundToInt(currentVideoDuration));
            string totalTime = FormatTime(Mathf.RoundToInt(totalVideoDuration));
            timeInfoText.text = currentTime + " / " + totalTime;
        }
    }

    public void Prepare(string url)
    {
        videoPlayer.url = url;
        videoPlayer.Prepare();
        Play();
    }

    public void Play()
    {
        playButton.gameObject.SetActive(false);
        pauseButton.gameObject.SetActive(true);
        pauseCalled = false;
        videoPlayer.Play();
    }

    public void Pause()
    {
        pauseButton.gameObject.SetActive(false);
        playButton.gameObject.SetActive(true);
        pauseCalled = true;
        videoPlayer.Pause();
    }

    public void PlayPause()
    {
        ShowUI();
        if (videoPlayer.isPrepared)
        {
            if (!pauseCalled)
            {
                Pause();
            }
            else
            {
                Play();
            }
        }
    }

    private void Update()
    {
        if (autoHideControlsTime > 0)
        {
            if (UserInteract())
            {
                ShowUI();
            }
            else
            {
                hideScreenTime += Time.deltaTime;
                if (hideScreenTime >= autoHideControlsTime)
                {
                    hideScreenTime = autoHideControlsTime;
                    HideControllers();
                }
            }
        }
    }

    private void HideControllers()
    {
        if (mainControllerUi != null)
        {
            mainControllerUi.SetActive(false);
            showingVolume = false;
            showingPlaybackSpeed = false;
            volumeSlider.gameObject.SetActive(false);
            playbackSpeed.gameObject.SetActive(false);
        }
    }

    public void ShowUI()
    {
        hideScreenTime = 0;
        if (mainControllerUi != null)
        {
            mainControllerUi.SetActive(true);
        }
    }

    public void Volume()
    {
        ShowUI();
        if (videoPlayer.audioOutputMode == VideoAudioOutputMode.AudioSource)
        {
            videoPlayer.GetComponent<AudioSource>().volume = volumeSlider.value;
        }
        else
        {
            videoPlayer.GetComponent<AudioSource>().volume = volumeSlider.value;
        }
    }

    public void Speed()
    {
        ShowUI();
        if (videoPlayer.canSetPlaybackSpeed)
        {
            if (playbackSpeed.value == 0)
            {
                videoPlayer.playbackSpeed = 0.5f;
            }
            else
            {
                videoPlayer.playbackSpeed = playbackSpeed.value;
            }
        }
    }


    public void VolumeSlider()
    {
        ShowUI();
        if (showingVolume)
        {
            showingVolume = false;
            volumeSlider.gameObject.SetActive(false);
        }
        else
        {
            showingVolume = true;
            volumeSlider.gameObject.SetActive(true);
        }
    }

    public void PlaybackSpeedSlider()
    {
        ShowUI();
        if (showingPlaybackSpeed)
        {
            showingPlaybackSpeed = false;
            playbackSpeed.gameObject.SetActive(false);
        }
        else
        {
            showingPlaybackSpeed = true;
            playbackSpeed.gameObject.SetActive(true);
        }
    }

    public void Stop()
    {
        videoPlayer.Stop();
        videoPlayer.url = null;
    }

    public void TrySkip(float value)
    {
        if (coroutine != null)
        {
            StopCoroutine(coroutine);
        }
        //StopAllCoroutines();
        coroutine = StartCoroutine(WaitSkip());
        ShowUI();
        SkipToPercent(value);
    }

    private IEnumerator WaitSkip()
    {
        progressEnabled = false;
        yield return new WaitForSeconds(1);
        progressEnabled = true;
    }

    private string FormatTime(int time)
    {
        int hours = time / 3600;
        int minutes = (time % 3600) / 60;
        int seconds = (time % 3600) % 60;
        if (hours == 0 && minutes != 0)
        {
            return minutes.ToString("00") + ":" + seconds.ToString("00");
        }
        else if (hours == 0 && minutes == 0)
        {
            return "00:" + seconds.ToString("00");
        }
        else
        {
            return hours.ToString("00") + ":" + minutes.ToString("00") + ":" + seconds.ToString("00");
        }
    }

    private bool UserInteract()
    {
        return false;
        if (Input.touches.Length >= 1)
        {
            if (!simpleTouchCheck)
            {
                Touch touch = Input.touches[0];
                Ray ray = Camera.main.ScreenPointToRay(touch.position);
                RaycastHit vHit;
                if (Physics.Raycast(ray.origin, ray.direction, out vHit))
                {
                    //if (vHit.transform.gameObject.layer.Equals("UI"))
                    //if (vHit.transform == mainControllerUi.transform)
                    if (vHit.transform.tag.Equals("Youtube"))
                    {
                        return true;
                    }
                }
                return false;
            }
            return true;
        }
        else
        {
            return false;
        }
    }

    private void SkipToPercent(float pct)
    {
        var frame = videoPlayer.frameCount * pct;
        //videoPlayer.Pause();
        videoPlayer.frame = (long)frame;
    }
}
