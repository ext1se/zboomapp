﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PreviewVideoPlayerButtonController : MonoBehaviour {

    public PreviewVideoPlayer VideoPlayer;
    public Image PlayImage;
    public Image PauseImage;

    public void FixedUpdate()
    {
        if (VideoPlayer.pauseCalled)
        {
            PlayImage.gameObject.SetActive(true);
            PauseImage.gameObject.SetActive(false);
        }
        else
        {
            PauseImage.gameObject.SetActive(true);
            PlayImage.gameObject.SetActive(false);
        }
    }
}
