﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartScene : MonoBehaviour
{
    public float Delay = 2.5f;

    IEnumerator Start()
    {
        yield return new WaitForSeconds(Delay);
        SceneManager.LoadSceneAsync(1);
    }
}
