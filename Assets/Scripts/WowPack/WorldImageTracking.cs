﻿using easyar;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WorldImageTracking : MonoBehaviour
{
    public ARSession Session;

    private ImageTrackerFrameFilter imageTracker;

    private void Awake()
    {
        imageTracker = Session.GetComponentInChildren<ImageTrackerFrameFilter>();

        ImageTargetController[] imageControllers = FindObjectsOfType<ImageTargetController>();
        foreach (ImageTargetController imageController in imageControllers)
        {
            AddTargetControllerEvents(imageController);
        }
    }

    public void Tracking(bool on)
    {
        imageTracker.enabled = on;
    }

    private void AddTargetControllerEvents(ImageTargetController controller)
    {
        if (!controller)
        {
            return;
        }

        controller.TargetFound += () =>
        {
            AssetBundleController[] assetBundleControllers = controller.GetComponentsInChildren<AssetBundleController>();
            foreach(AssetBundleController assetBundleController in assetBundleControllers)
            {
                assetBundleController.LoadAssetBundle();
            }

            YoutubePlayer[] youtubePlayers = controller.GetComponentsInChildren<YoutubePlayer>();
            foreach (YoutubePlayer youtubePlayer in youtubePlayers)
            {
                youtubePlayer.Play();
            }

            Debug.LogFormat("Found target {{id = {0}, name = {1}}}", controller.Target.runtimeID(), controller.Target.name());
        };
        controller.TargetLost += () =>
        {
            Debug.LogFormat("Lost target {{id = {0}, name = {1}}}", controller.Target.runtimeID(), controller.Target.name());
        };
        controller.TargetLoad += (Target target, bool status) =>
        {
            Debug.LogFormat("Load target {{id = {0}, name = {1}, size = {2}}} into {3} => {4}", target.runtimeID(), target.name(), controller.Size, controller.Tracker.name, status);
        };
        controller.TargetUnload += (Target target, bool status) =>
        {
            Debug.LogFormat("Unload target {{id = {0}, name = {1}}} => {2}", target.runtimeID(), target.name(), status);
        };
    }
}
