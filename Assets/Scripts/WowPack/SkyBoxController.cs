﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkyBoxController : MonoBehaviour
{
    public float Speed = 10f;

    public Skybox skybox;
    private float currentRotation = 0;

    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        //currentRotation += Speed * Time.deltaTime;
            skybox.material.SetFloat("_Rotation", Time.time * Speed);
    }
}
