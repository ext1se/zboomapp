﻿using easyar;
using System;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class CameraController : MonoBehaviour
{
    public ARSession ARSession;
    public PreviewController PreviewController;

    private VideoCameraDevice m_videoCamera;
    private VideoRecorder m_videoRecorder;
    private CameraRecorder m_cameraRecorder;
    private string m_fileName;
    private string m_appName = "WoW Pack";

    private void Awake()
    {
        m_videoCamera = ARSession.GetComponentInChildren<VideoCameraDevice>();
        m_videoRecorder = FindObjectOfType<VideoRecorder>();
        m_videoRecorder.FilePathType = WritablePathType.PersistentDataPath;
        m_videoRecorder.StatusUpdate += (status, msg) =>
        {
            if (status == RecordStatus.OnStarted)
            {
                //GUIPopup.EnqueueMessage("Recording start", 5);
            }
            if (status == RecordStatus.FailedToStart || status == RecordStatus.FileFailed || status == RecordStatus.LogError)
            {
                //GUIPopup.EnqueueMessage("Recording Error: " + status + ", details: " + msg, 2);
            }
            Debug.Log("RecordStatus: " + status + ", details: " + msg);
        };
    }

    private void OnDestroy()
    {

    }

    public void RecorderStart()
    {
        if (!m_videoRecorder)
        {
            return;
        }
        if (!m_videoRecorder.IsReady)
        {
            return;
        }

        m_fileName = m_appName + DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss") + ".mp4";
        m_videoRecorder.FilePath = m_fileName;
        m_videoRecorder.StartRecording();
        m_cameraRecorder = Camera.main.gameObject.AddComponent<CameraRecorder>();
        m_cameraRecorder.Setup(m_videoRecorder, null);
    }

    public void RecorderStop()
    {
        if (!m_videoRecorder)
        {
            return;
        }
        if (m_videoRecorder.StopRecording())
        {
            /* GUIPopup.EnqueueMessage("Recording finished, video saved to Unity Application.persistentDataPath" + Environment.NewLine +
                 "Filename: " + filePath + Environment.NewLine +
                 "PersistentDataPath: " + Application.persistentDataPath + Environment.NewLine +
                 "You can change sample code if you prefer to record videos into system album", 8);*/

            /*GUIPopup.EnqueueMessage(
               "Filename: " + filePath + Environment.NewLine +
               "Path: " + Application.persistentDataPath + Environment.NewLine, 3);*/
            AdjustVideoAndPlay();
        }
        else
        {
            GUIPopup.EnqueueMessage("Recording failed", 5);
        }
        if (m_cameraRecorder)
        {
            m_cameraRecorder.Destroy();
        }
    }

    private void AdjustVideoAndPlay()
    {
        // TODO: Костыль
        // Сохраняем в Галерею и удаляем из папки приложения
        string fullPath = Application.persistentDataPath + "/" + m_fileName;
        NativeGallery.SaveVideoToGallery(fullPath, m_appName, m_fileName);

        PreviewController.UpdatePreviewVideo(fullPath);
    }

    public void Snapshot()
    {
        m_fileName = m_appName + DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss") + ".png";

        var oneShot = Camera.main.gameObject.AddComponent<OneShot>();
        oneShot.Shot(false, (texture) =>
        {
            NativeGallery.SaveImageToGallery(texture, m_appName, m_fileName);
            PreviewController.UpdatePreviewImageWithAspectRatio(texture);
        });
    }

    public void NextCamera()
    {
        if (!m_videoCamera || m_videoCamera.Device == null)
        {
            return;
        }

        if (CameraDevice.cameraCount() == 0)
        {
            //GUIPopup.EnqueueMessage("Camera unavailable", 3);
            //videoCamera.Close();
            return;
        }

        int index = m_videoCamera.Device.index();
        index = (index + 1) % CameraDevice.cameraCount();
        m_videoCamera.CameraOpenMethod = VideoCameraDevice.CameraDeviceOpenMethod.DeviceIndex;
        m_videoCamera.CameraIndex = index;

        m_videoCamera.Close();
        m_videoCamera.Open();
    }
}