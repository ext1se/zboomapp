using Doozy.Engine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BottomMenuController : MonoBehaviour
{
    public CameraController CameraController;

    public UIView MenuView;
    public UIView BackButton;
    public UIView SwitchCameraButton;
    public UIView TakePhotoButton;
    public UIView StartVideoButton;
    public UIView StopVideoButton;
    public UIView PhotoButton;
    public UIView VideoButton;
    public UIView MenuCloseButton;
    public UIView MenuOpenButton;

    private void Start()
    {
        SetPhoto();
    }

    public void OpenMenu()
    {
        MenuView.SetVisibility(true);
        MenuOpenButton.gameObject.SetActive(false);
    }

    public void CloseMenu()
    {
        MenuView.SetVisibility(false);
        MenuOpenButton.gameObject.SetActive(true);
    }

    public void SwitchCamera()
    {
        CameraController.NextCamera();
    }

    public void TakePhoto()
    {
        CameraController.Snapshot();
    }

    public void StartVideo()
    {
        BackButton.gameObject.SetActive(false);
        SwitchCameraButton.gameObject.SetActive(false);
        PhotoButton.gameObject.SetActive(false);
        VideoButton.gameObject.SetActive(false);
        MenuCloseButton.gameObject.SetActive(false);
        TakePhotoButton.gameObject.SetActive(false);
        StartVideoButton.gameObject.SetActive(false);
        StopVideoButton.gameObject.SetActive(true);

        CameraController.RecorderStart();
    }

    public void StopVideo()
    {
        BackButton.gameObject.SetActive(true);
        SwitchCameraButton.gameObject.SetActive(true);
        PhotoButton.gameObject.SetActive(true);
        VideoButton.gameObject.SetActive(false);
        MenuCloseButton.gameObject.SetActive(true);
        TakePhotoButton.gameObject.SetActive(false);
        StartVideoButton.gameObject.SetActive(true);
        StopVideoButton.gameObject.SetActive(false);

        CameraController.RecorderStop();
    }

    public void SetVideo()
    {
        PhotoButton.gameObject.SetActive(true);
        VideoButton.gameObject.SetActive(false);
        TakePhotoButton.gameObject.SetActive(false);
        StartVideoButton.gameObject.SetActive(true);
        StopVideoButton.gameObject.SetActive(false);
    }

    public void SetPhoto()
    {
        PhotoButton.gameObject.SetActive(false);
        VideoButton.gameObject.SetActive(true);
        TakePhotoButton.gameObject.SetActive(true);
        StartVideoButton.gameObject.SetActive(false);
        StopVideoButton.gameObject.SetActive(false);
    }

}
