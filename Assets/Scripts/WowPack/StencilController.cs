﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StencilController : MonoBehaviour
{
    public Shader Shader;
    public int StencilRef = 1;
    public bool IsTransformed = true;

    private void Start()
    {
        if (Shader == null)
        {
            Shader = Shader.Find("Portal/ReadMobile");
        }

        Renderer[] renderers = GetComponentsInChildren<Renderer>();
        foreach (Renderer renderer in renderers)
        {
            Material[] materials = renderer.materials;
            foreach (Material m in materials)
            {
                string shaderName = m.shader.name;
                if (shaderName.Contains("Portal"))
                {
                    m.SetFloat("_StencilRef", StencilRef);
                    continue;
                }
                m.shader = Shader;
                m.SetFloat("_StencilRef", StencilRef);
            }
        }
    }
}
