﻿using UnityEngine;
using UnityEngine.UI;

public class ScreenOrientationController : MonoBehaviour
{
    public Canvas GameInterface;
    
    protected void Start()
    {
        OnScreenTransformDimensionsChange();
    }

    protected void OnRectTransformDimensionsChange()
    {
        if (!GameInterface)
        {
            return;
        }
        OnScreenTransformDimensionsChange();
    }

    private void OnScreenTransformDimensionsChange()
    {
        CanvasScaler scaler = GameInterface.GetComponent<CanvasScaler>();
        if (Screen.width > Screen.height)
        {
            scaler.matchWidthOrHeight = 0.65f;
        }
        else
        {
            scaler.matchWidthOrHeight = 0.5f;
        }
    }

    public interface IScreenOrientation
    {
        void OnScreenCameraTransformDimensionsChange();
        void OnScreenOverlayTransformDimensionsChange();
    }
}
