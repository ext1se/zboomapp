﻿using easyar;
using Michsky.UI.ModernUIPack;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class AssetBundleController : MonoBehaviour
{
    public enum DownloadState
    {
        IDLE, 
        PROCESS,
        SUCCESS,
        ERROR
    }

    public UIManagerProgressBarLoop ProgressBar;
    public string Name = "AssetBundle";
    public int Version = 1;
    public string URL;
    public bool CheckExistShader = false;
    public bool UseShadow = false;

    [HideInInspector]
    public DownloadState State = DownloadState.IDLE;

    private void Start()
    {
        if (ProgressBar == null)
        {
            ProgressBar = FindObjectOfType<UIManagerProgressBarLoop>();
        }
    }

    public void LoadAssetBundle()
    {
        if (URL != null)
        {
            if (State == DownloadState.IDLE || State == DownloadState.ERROR)
            {
                State = DownloadState.PROCESS;
                StartCoroutine(Load());
            }
        }
    }

    private IEnumerator Load()
    {
        ProgressBar.gameObject.SetActive(true);
        using (WWW www = WWW.LoadFromCacheOrDownload(URL, Version))
        //using (WWW www = WWW.LoadFromCacheOrDownload(URL, new CachedAssetBundle(Name, new Hash128())))
        //using (WWW www = WWW.LoadFromCacheOrDownload(assetBundleName, new CachedAssetBundle(System.Guid.NewGuid().ToString(), new Hash128())))
        //using (WWW www = new WWW(assetBundleName))
        {
            yield return www;   // Wait for download to complete
            if (www.error != null)
            {
                ProgressBar.gameObject.SetActive(false);
                State = DownloadState.ERROR;
                Debug.Log("WWW download had an error/ check internet connection" + www.error);
                GUIPopup.EnqueueMessage("ERROR ASSET BUNDLE", 2);
            }
            else
            {
                AssetBundle bundle = www.assetBundle;   // Load and retrieve the AssetBundle
                if (bundle == null)
                {
                    ProgressBar.gameObject.SetActive(false);
                    State = DownloadState.ERROR;
                    yield break;
                }
                string assetName = bundle.GetAllAssetNames()[0]; ////////////
                if (bundle.LoadAsset(assetName) != null)
                {
                    var loadAsset = bundle.LoadAsset<GameObject>(assetName);
                    GameObject newObject = Instantiate(loadAsset) as GameObject;
                    if (UseShadow)
                    {
                        MeshRenderer[] meshes = newObject.GetComponentsInChildren<MeshRenderer>();
                        foreach (MeshRenderer mesh in meshes)
                        {
                            mesh.receiveShadows = false;
                            mesh.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
                        }
                    }
                    if (CheckExistShader)
                    {
                        Renderer[] renderers = newObject.GetComponentsInChildren<Renderer>();
                        foreach (Renderer renderer in renderers)
                        {
                            Material[] materials = renderer.materials;
                            foreach (Material m in materials)
                            {
                                var shaderName = m.shader.name;
                                var newShader = Shader.Find(shaderName);
                                if (newShader != null)
                                {
                                    m.shader = newShader;
                                    //m.shader = Shader.Find("Mobile/VertexLit");
                                }
                                else
                                {
                                    //GUIPopup.EnqueueMessage("ERROR ASSET BUNDLE", 2);
                                    m.shader = Shader.Find("Standart");
                                    Debug.LogWarning("unable to refresh shader: " + shaderName + " in material " + m.name);
                                }
                            }
                        }
                    }

                    newObject.transform.parent = gameObject.transform;
                    newObject.transform.localPosition = Vector3.zero;
                    newObject.transform.rotation = Quaternion.identity;
                    newObject.transform.localRotation = Quaternion.identity;
                    bundle.Unload(false);
                }
                ProgressBar.gameObject.SetActive(false);
                State = DownloadState.SUCCESS;
                //bundle.Unload(false); // Unload the AssetBundles compressed contents to conserve memory
                www.Dispose();  // Frees the memory from the web stream
            }
        }
    }
}
