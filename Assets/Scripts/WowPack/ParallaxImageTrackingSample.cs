﻿//================================================================================================================================
//
//  Copyright (c) 2015-2019 VisionStar Information Technology (Shanghai) Co., Ltd. All Rights Reserved.
//  EasyAR is the registered trademark or trademark of VisionStar Information Technology (Shanghai) Co., Ltd in China
//  and other countries for the augmented reality technology developed by VisionStar Information Technology (Shanghai) Co., Ltd.
//
//================================================================================================================================

using easyar;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace ImageTracking_ImageTarget
{
    public class ParallaxImageTrackingSample : MonoBehaviour
    {
        public ARSession Session;
        public Text Status;

        private Dictionary<ImageTargetController, bool> m_imageTargetControllers = new Dictionary<ImageTargetController, bool>();
        private ImageTargetController m_controllerOxygen;
        private ImageTargetController m_controllerDrone;
        private ImageTrackerFrameFilter m_imageTracker;
        private VideoCameraDevice m_cameraDevice;

        private void Awake()
        {
            m_imageTracker = Session.GetComponentInChildren<ImageTrackerFrameFilter>();
            m_cameraDevice = Session.GetComponentInChildren<VideoCameraDevice>();

            // targets from scene
            m_controllerOxygen = GameObject.Find("ImageTargetOxygen").GetComponent<ImageTargetController>();
            m_controllerDrone = GameObject.Find("ImageTargetDrone").GetComponent<ImageTargetController>();
            m_imageTargetControllers[m_controllerOxygen] = false;
            m_imageTargetControllers[m_controllerDrone] = false;
            AddTargetControllerEvents(m_controllerOxygen);
            AddTargetControllerEvents(m_controllerDrone);

            //CreateTargets();
        }

        private void Update()
        {
            bool isFront = false;
            if(m_cameraDevice.Device != null)
            {
                using (var cameraParameters = m_cameraDevice.Device.cameraParameters())
                {
                    if (cameraParameters.cameraDeviceType() == CameraDeviceType.Front)
                    {
                        isFront = true;
                    }
                }
            }

            var statusText = "CenterMode: " + Session.CenterMode + Environment.NewLine +
                "CenterTarget: " + (Session.CenterTarget ? Session.CenterTarget.name : null) + Environment.NewLine +
                "HorizontalFlip: " + (isFront ? Session.HorizontalFlipFront : Session.HorizontalFlipNormal) + Environment.NewLine +
                "Camera: " + (m_cameraDevice && m_cameraDevice.enabled ? "On" : "Off") + Environment.NewLine +
                "Tracking: " + (m_imageTracker && m_imageTracker.enabled ? "On" : "Off") + Environment.NewLine + Environment.NewLine +
                "Target Load Status:" + Environment.NewLine;

            foreach (var item in m_imageTargetControllers)
            {
                statusText += "\t" + item.Key.gameObject.name + ": " + item.Value + Environment.NewLine;
            }

            Status.text = statusText;
        }

        public void Tracking(bool on)
        {
            m_imageTracker.enabled = on;
        }

        public void UnloadTargets()
        {
            foreach (var item in m_imageTargetControllers)
            {
                item.Key.Tracker = null;
            }
        }

        public void LoadTargets()
        {
            foreach (var item in m_imageTargetControllers)
            {
                item.Key.Tracker = m_imageTracker;
            }
        }

        public void SwitchCenterMode()
        {
            while (true)
            {
                Session.CenterMode = (ARSession.ARCenterMode)(((int)Session.CenterMode + 1) % Enum.GetValues(typeof(ARSession.ARCenterMode)).Length);
                if (Session.CenterMode == ARSession.ARCenterMode.SpecificTarget)
                {
                    Session.CenterTarget = m_controllerOxygen;
                }
                if (Session.CenterMode == ARSession.ARCenterMode.FirstTarget ||
                    Session.CenterMode == ARSession.ARCenterMode.Camera ||
                    Session.CenterMode == ARSession.ARCenterMode.SpecificTarget)
                {
                    break;
                }
            }
        }

        public void EnableCamera(bool enable)
        {
            m_cameraDevice.enabled = enable;
        }

        public void SwitchHFlipMode()
        {
            if (m_cameraDevice.Device == null)
            {
                return;
            }
            using (var cameraParameters = m_cameraDevice.Device.cameraParameters())
            {
                if (cameraParameters.cameraDeviceType() == CameraDeviceType.Front)
                {
                    Session.HorizontalFlipFront = (ARSession.ARHorizontalFlipMode)(((int)Session.HorizontalFlipFront + 1) % Enum.GetValues(typeof(ARSession.ARHorizontalFlipMode)).Length);
                }
                else
                {
                    Session.HorizontalFlipNormal = (ARSession.ARHorizontalFlipMode)(((int)Session.HorizontalFlipNormal + 1) % Enum.GetValues(typeof(ARSession.ARHorizontalFlipMode)).Length);
                }
            }
        }

        public void NextCamera()
        {
            if (!m_cameraDevice || m_cameraDevice.Device == null)
            {
                return;
            }
            if (CameraDevice.cameraCount() == 0)
            {
                GUIPopup.EnqueueMessage("Camera unavailable", 3);
                m_cameraDevice.Close();
                return;
            }

            var index = m_cameraDevice.Device.index();
            index = (index + 1) % CameraDevice.cameraCount();
            m_cameraDevice.CameraOpenMethod = VideoCameraDevice.CameraDeviceOpenMethod.DeviceIndex;
            m_cameraDevice.CameraIndex = index;
            GUIPopup.EnqueueMessage("Switch to camera index: " + index, 3);

            m_cameraDevice.Close();
            m_cameraDevice.Open();
        }

        private void CreateTargets()
        {
            // dynamically load from image (*.jpg, *.png)
            var targetController = CreateTargetNode("ImageTarget-argame00");
            targetController.Tracker = m_imageTracker;
            targetController.SourceType = ImageTargetController.DataSource.ImageFile;
            targetController.ImageFileSource.PathType = PathType.StreamingAssets;
            targetController.ImageFileSource.Path = "sightplus/argame00.jpg";
            targetController.ImageFileSource.Name = "argame00";
            targetController.ImageFileSource.Scale = 0.1f;

            GameObject duck02 = Instantiate(Resources.Load("duck02")) as GameObject;
            duck02.transform.parent = targetController.gameObject.transform;

            // dynamically load from json string
            var imageJosn = JsonUtility.FromJson<ImageJson>(@"
            {
                ""images"" :
                [
                    {
                        ""image"" : ""sightplus/argame01.png"",
                        ""name"" : ""argame01""
                    },
                    {
                        ""image"" : ""sightplus/argame02.jpg"",
                        ""name"" : ""argame02"",
                        ""scale"" : 0.2
                    },
                    {
                        ""image"" : ""sightplus/argame03.jpg"",
                        ""name"" : ""argame03"",
                        ""scale"" : 1,
                        ""uid"" : ""uid string will be ignored""
                    }
                ]
            }");

            foreach (var image in imageJosn.images)
            {
                targetController = CreateTargetNode("ImageTarget-" + image.name);
                targetController.Tracker = m_imageTracker;
                targetController.ImageFileSource.PathType = PathType.StreamingAssets;
                targetController.ImageFileSource.Path = image.image;
                targetController.ImageFileSource.Name = image.name;
                targetController.ImageFileSource.Scale = image.scale;

                var duck03 = Instantiate(Resources.Load("duck03")) as GameObject;
                duck03.transform.parent = targetController.gameObject.transform;
            }
        }

        private ImageTargetController CreateTargetNode(string targetName)
        {
            GameObject go = new GameObject(targetName);
            var targetController = go.AddComponent<ImageTargetController>();
            AddTargetControllerEvents(targetController);
            m_imageTargetControllers[targetController] = false;
            return targetController;
        }

        private void AddTargetControllerEvents(ImageTargetController controller)
        {
            if (!controller)
            {
                return;
            }

            controller.TargetFound += () =>
            {
                Debug.LogFormat("Found target {{id = {0}, name = {1}}}", controller.Target.runtimeID(), controller.Target.name());
            };
            controller.TargetLost += () =>
            {
                Debug.LogFormat("Lost target {{id = {0}, name = {1}}}", controller.Target.runtimeID(), controller.Target.name());
            };
            controller.TargetLoad += (Target target, bool status) =>
            {
                m_imageTargetControllers[controller] = status ? true : m_imageTargetControllers[controller];
                Debug.LogFormat("Load target {{id = {0}, name = {1}, size = {2}}} into {3} => {4}", target.runtimeID(), target.name(), controller.Size, controller.Tracker.name, status);
            };
            controller.TargetUnload += (Target target, bool status) =>
            {
                m_imageTargetControllers[controller] = status ? false : m_imageTargetControllers[controller];
                Debug.LogFormat("Unload target {{id = {0}, name = {1}}} => {2}", target.runtimeID(), target.name(), status);
            };
        }

        [Serializable]
        public class ImageJson
        {
            public ImageFile[] images;
        }

        [Serializable]
        public class ImageFile
        {
            public string image;
            public string name;
            public float scale = 0.1f;
        }
    }
}
