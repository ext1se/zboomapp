﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WebController : MonoBehaviour
{
    public string URL = "";

    public void OpenBrowser()
    {
        Application.OpenURL(URL);
    }

    public void OpenBrowser(string url)
    {
        Application.OpenURL(url);
    }
}
