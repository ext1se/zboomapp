using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LocalRepositoryController : MonoBehaviour, IRepository
{
    public const string KEY_SCORE = "KEY_SCORE";
    public const string KEY_USER = "KEY_USER";

    protected void Start()
    {
        
    }

    public ZboomUser GetUser(string id = null, IResultListener<ZboomUser> resultListener = null)
    {
        string jsonUser = PlayerPrefs.GetString(KEY_USER, "");
        if (jsonUser.Length > 1)
        {
            return JsonUtility.FromJson<ZboomUser>(jsonUser);
        }
        else
        {
            return new ZboomUser();
        }
    }

    public void SaveScore(ZboomUser user, int score, IResultListener<ZboomUser> resultListener = null)
    {
        PlayerPrefs.SetInt(KEY_SCORE, score);
    }

    public void SaveUser(ZboomUser user, IResultListener<ZboomUser> resultListener = null)
    {
        string json = JsonUtility.ToJson(user);
        PlayerPrefs.SetString(KEY_USER, json);
    }

    public void UpdateUser(ZboomUser user, IResultListener<ZboomUser> resultListener = null)
    {
        SaveUser(user, resultListener);
    }
}
