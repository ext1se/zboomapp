﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RobotController : HeroController
{
    private Animator m_Animator;

    private void Start()
    {
        m_Animator = GetComponent<Animator>();
    }

    public override void GetToken()
    {
        int dance = Random.Range(0, 3);
        switch (dance)
        {
            case (0):
                m_Animator.SetTrigger("dance_salsa");
                break;
            case (1):
                m_Animator.SetTrigger("dance_twist");
                break;
            case (2):
                m_Animator.SetTrigger("dance_samba");
                break;
        }
    }
}
