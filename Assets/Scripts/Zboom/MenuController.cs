using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuController : MonoBehaviour
{
    public GameObject ServerRepositoryGameObject;
    public GameObject LocalRepositoryGameObject;

    [HideInInspector]
    public ZboomUser CurrentUser;
    public IRepository ServerRepositoryController;
    public IRepository LocalRepositoryController;

    public TextMeshProUGUI TextContact;
    public TextMeshProUGUI TextBalance;

    protected virtual void OnValidate()
    {
        ValidateHelper.Validate<IRepository>(ServerRepositoryGameObject);
        ValidateHelper.Validate<IRepository>(LocalRepositoryGameObject);
    }

    protected void Start()
    {
        ServerRepositoryController = ServerRepositoryGameObject.GetComponent<IRepository>();
        LocalRepositoryController = LocalRepositoryGameObject.GetComponent<IRepository>();

        CurrentUser = LocalRepositoryController.GetUser();
        if (CurrentUser == null || !CurrentUser.IsAuthorizedUser())
        {
            CurrentUser = new ZboomUser();
            CurrentUser.contact = "Guest";
        }
        TextContact.text = CurrentUser.contact;
        TextBalance.text = "������: " + CurrentUser.score;
    }

    public void Logout()
    {
        ZboomUser user = new ZboomUser();
        user.id = "-1";
        LocalRepositoryController.UpdateUser(user);
        SceneManager.LoadScene(0);
        //TODO
    }
}
