﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class HeroController : MonoBehaviour
{
    public abstract void GetToken();
}
