﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAtCamera : MonoBehaviour
{
    public bool IsLootAtCamera = false;
    private Transform m_MainCameraTransform;

    private void Start()
    {
        m_MainCameraTransform = Camera.main.transform;
    }

    public void Update()
    {
        if (IsLootAtCamera)
        {
            /*
            Vector3 direction = m_MainCameraTransform.position - transform.position;
            Quaternion rotation = Quaternion.LookRotation(direction);
            rotation.x = 0;
            rotation.z = 0;
            transform.rotation = rotation;
            */

            Vector3 targetPostition = new Vector3(m_MainCameraTransform.position.x,
                                        transform.position.y,
                                        m_MainCameraTransform.position.z);
            transform.LookAt(targetPostition);
        }
    }
}
