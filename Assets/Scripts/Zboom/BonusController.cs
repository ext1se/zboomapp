﻿using easyar;
using Michsky.UI.ModernUIPack;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BonusController : MonoBehaviour
{
    public TokenGeneratorController TokenGeneratorController;
    //public ImageTargetController ImageTargetController; //
    public InterfaceAnimManager InfoInterface;
    public InterfaceAnimManager ErrorInterface;
    public ProgressBar ProgressBar;
    public Button MagnetButton;

    public float Speed = 10f;

    protected virtual void Start()
    {
        ProgressBar.currentPercent = 100;
    }

    protected void Update()
    {
        if (ProgressBar.currentPercent < 100)
        {
            ProgressBar.currentPercent += (Speed * Time.deltaTime);
            ProgressBar.UpdateUI();
        }
    }

    public void Show()
    {
        SetState(InfoInterface, CSFHIAnimableState.disappeared);
        InfoInterface.currentState = CSFHIAnimableState.disappeared;

        InfoInterface.gameObject.SetActive(true);
        InfoInterface.startAppear();

        SetState(ErrorInterface, CSFHIAnimableState.disappeared);
        ErrorInterface.currentState = CSFHIAnimableState.disappeared;
        ErrorInterface.gameObject.SetActive(false);
    }

    public void Hide()
    {
        SetState(ErrorInterface, CSFHIAnimableState.disappeared);
        ErrorInterface.currentState = CSFHIAnimableState.disappeared;
        ErrorInterface.gameObject.SetActive(false);

        SetState(InfoInterface, CSFHIAnimableState.disappeared);
        InfoInterface.currentState = CSFHIAnimableState.disappeared;
        InfoInterface.gameObject.SetActive(false);

        StopAllCoroutines();
    }

    private void SetState(InterfaceAnimManager animManager, CSFHIAnimableState state)
    {
        foreach (InterfaceAnmElement element in animManager.elementsList)
        {
            element.gameObject.SetActive(true);
            element.enabled = true;
            element.currentState = state;
        }
    }

    public void Magnetize()
    {
        if (ProgressBar.currentPercent >= 100)
        {
            ProgressBar.currentPercent = 0;
            ProgressBar.UpdateUI();
            TokenGeneratorController.MagnetizeTokens();
        }
        else
        {
            StartCoroutine(ShowErrorInterface());
        }
    }

    private IEnumerator ShowErrorInterface()
    {
        InfoInterface.startDisappear();
        yield return new WaitForSeconds(1.5f);
        ErrorInterface.gameObject.SetActive(true);
        ErrorInterface.startAppear();
        yield return new WaitForSeconds(3);
        ErrorInterface.startDisappear();
        yield return new WaitForSeconds(1.5f);
        InfoInterface.startAppear();
    }
}
