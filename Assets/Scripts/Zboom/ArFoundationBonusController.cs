using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

public class ArFoundationBonusController : BonusController
{
    public ARTrackedImageManager TrackedImageManager;
    public string IdImage;
    public GameObject BonusObject;

    private bool m_IsVisible = false;

    protected override void Start()
    {
        base.Start();
        BonusObject.SetActive(false);
        TrackedImageManager.trackedImagesChanged += OnTrackedImagesChanged;
    }

    void OnTrackedImagesChanged(ARTrackedImagesChangedEventArgs eventArgs)
    {
        foreach (ARTrackedImage image in eventArgs.added)
        {
            if (image.referenceImage.name.Equals(IdImage))
            {
                transform.parent = image.transform;

                var minLocalScalar = Mathf.Min(image.size.x, image.size.y);
                m_IsVisible = true;
                BonusObject.SetActive(true);
                BonusObject.transform.localScale = new Vector3(minLocalScalar, minLocalScalar, minLocalScalar);
                Show();
                return;
            }
        }

        foreach (ARTrackedImage image in eventArgs.updated)
        {
            if (image.referenceImage.name.Equals(IdImage))
            {
                if (image.trackingState == TrackingState.Tracking)
                {
                    if (!m_IsVisible)
                    {
                        m_IsVisible = true;
                        BonusObject.SetActive(true);
                        Show();
                    }
                    BonusObject.transform.SetPositionAndRotation(image.transform.position, image.transform.rotation);
                }
                else
                {
                    m_IsVisible = false;
                    BonusObject.SetActive(false);
                    Hide();
                }
                return;
            }
        }

        foreach (ARTrackedImage image in eventArgs.removed)
        {
            if (image.referenceImage.guid.Equals(IdImage))
            {
                m_IsVisible = false;
                BonusObject.SetActive(false);
                return;
            }
            //Destroy(BonusObject);
        }
    }

    void OnEnable()
    {
        //TrackedImageManager.trackedImagesChanged += OnTrackedImagesChanged;
    }

    void OnDisable()
    {
        //TrackedImageManager.trackedImagesChanged -= OnTrackedImagesChanged;
    }
}
