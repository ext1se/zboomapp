﻿using easyar;
using Firebase.Database;
using Firebase.Extensions;
using Michsky.UI.ModernUIPack;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.XR.ARFoundation;
using Random = UnityEngine.Random;

[Serializable]
public class ArFoundationWorldController : SlamGameWorldController
{
    public ARPlaneManager PlaneManager;
    public TouchController TouchControl;
    //public GameObject TouchControl;

    private bool m_CanMove = true;
    private bool m_IsTransparent = false;
    private bool m_IsRendered = true;

    protected override void Awake()
    {
        base.Awake();
        TouchControl.TurnOn(TouchControl.gameObject.transform, Camera.main, false, false, false, false);
    }

    protected override void Start()
    {
        base.Start();
    }

    protected override void Update()
    {
        base.Update();
    }

    protected override void UpdateSetRobotPosition()
    {
        base.UpdateSetRobotPosition();
        if (CheckTouch())
        {
            Ray ray = m_CameraTarget.ScreenPointToRay(Input.touches[0].position);
            RaycastHit hitInfo;
            if (Physics.Raycast(ray, out hitInfo))
            {
                TouchControl.gameObject.SetActive(true);
                TouchControl.transform.position = hitInfo.point;
                Vector3 targetPostition = new Vector3(m_CameraTarget.transform.position.x,
                                    TouchControl.transform.position.y,
                                    m_CameraTarget.transform.position.z);
                TouchControl.transform.LookAt(targetPostition);
                GameUiController.SetRobotPosition(true);
            };
        }
    }

    public override void SetStateScanArea()
    {
        base.SetStateScanArea();
        StartCoroutine(UpdateVisibleArea());
    }

    private IEnumerator UpdateVisibleArea()
    {
        yield return new WaitForSeconds(1f);
        bool isVisible;
        if (PlaneManager.trackables.count > 1)
        {
            isVisible = true;
        }
        else
        {
            isVisible = false;
        }
        GameUiController.ScanArea(isVisible);
        StartCoroutine(UpdateVisibleArea());
    }

    public override void SetStateSetRobotPosition()
    {
        base.SetStateSetRobotPosition();
    }

    public override void SetStateStartGame()
    {
        base.SetStateStartGame();
        ShowMap(false);
        //TransparentMesh(true);
    }


    public override void SetStatePlayGame()
    {
        base.SetStatePlayGame();
        TouchControl.gameObject.SetActive(true);
    }

    public override void SetStateFinishGame()
    {
        base.SetStateFinishGame();
        TouchControl.gameObject.SetActive(false);
    }

    //------------------------------------------------//

    public void RotateModel(float value)
    {
        TouchControl.transform.rotation = Quaternion.Euler(
            TouchControl.transform.rotation.x,
            value,
            TouchControl.transform.rotation.z
        );
        //TouchControl.transform.RotateAround(Vector3.up, value);
    }

    public void ScaleModel(float value)
    {
        TouchControl.transform.localScale = new Vector3(value, value, value);
    }

    //------------------------------------------------//

    public void ShowMap(bool value)
    {
        if (PlaneManager == null)
        {
            return;
        }
        PlaneManager.SetTrackablesActive(value);
        PlaneManager.enabled = value;
        foreach (ARPlane plane in PlaneManager.trackables)
        {
            plane.GetComponent<MeshCollider>().enabled = value;
        }
    }

    public void ShowMap()
    {
        m_IsRendered = !m_IsRendered;
        ShowMap(m_IsRendered);
    }

    public void TransparentMesh(bool value)
    {
        if (PlaneManager == null)
        {
            return;
        }
        foreach (ARPlane plane in PlaneManager.trackables)
        {
            plane.GetComponent<Renderer>().material.color = value ? Color.white : Color.clear;
        }
    }

    public void TransparentMesh()
    {
        m_IsTransparent = !m_IsTransparent;
        TransparentMesh(m_IsTransparent);
    }

    public void EnableMove(bool move)
    {
        m_CanMove = move;
    }
}