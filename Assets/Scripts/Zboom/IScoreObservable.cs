using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IScoreObservable
{
    public void AddObserver(IScoreObserver scoreObserver);

    public void RemoveObserver(IScoreObserver scoreObserver);

    public void NotifyObservers();
}
