using Michsky.UI.ModernUIPack;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameUiArController : MonoBehaviour, IScoreObserver
{
    public SlamGameWorldController WorldController;
    public InterfaceAnimManager WindowWelcome;
    public InterfaceAnimManager WindowScore;
    public Text TextScore;

    private void Start()
    {
        if (WorldController is IScoreObservable scoreObservable)
        {
            scoreObservable.AddObserver(this);
        }
        Clear();
    }

    private void Clear()
    {
        WindowWelcome.gameObject.SetActive(false);
        WindowScore.gameObject.SetActive(false);
    }

    public void StartGame()
    {
        WindowWelcome.gameObject.SetActive(true);
        WindowWelcome.startAppear(true);
    }

    public void PlayGame()
    {
        WindowWelcome.startDisappear(true);
        WindowScore.gameObject.SetActive(true);
        WindowScore.startAppear(true);
    }

    public void UpdateScore(int score)
    {
        TextScore.text = "" + score;
    }
}
