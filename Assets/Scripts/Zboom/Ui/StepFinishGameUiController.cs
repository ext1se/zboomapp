using Firebase.Database;
using Firebase.Extensions;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StepFinishGameUiController : BaseGameUiController
{
    private const string TEXT_SUCCESS = "��������� ������� �������� � ��������!";
    private const string TEXT_ERROR = "��������� ������, ���������� �����.";

    public GameObject MainPanel;
    public TextMeshProUGUI ResultText;
    public TextMeshProUGUI ResultOperationText;
    public TextMeshProUGUI UserName;
    public GameObject ProgressPanel;
    public GameObject ButtonRefresh;
    public GameObject ButtonRestart;
    public GameObject ButtonBack;

    private ZboomUser m_CurrentUser;

    protected override void Start()
    {
        base.Start();
    }

    protected override void Activate()
    {
        MainPanel.SetActive(true);
    }

    protected override void Deactivate()
    {
        MainPanel.SetActive(false);
    }

    public void SetResult(ZboomUser user)
    {
        m_CurrentUser = user;
        ResultText.text = "" + user.score;

        ProgressPanel.SetActive(false);
        ButtonRefresh.SetActive(false);
        ButtonRestart.SetActive(true);
        ButtonBack.SetActive(true);

        if (user.IsAuthorizedUser())
        {
            UserName.gameObject.SetActive(true);
            UserName.text = user.contact;
            user.date_updated = DateTime.Now.ToString("MM/dd/yyyy h:mm");
            SaveResultToServerDb(user);
        }
        else
        {
            UserName.gameObject.SetActive(false);
        }
    }

    private void SaveResultToServerDb(ZboomUser user)
    {
        StartSaveResult();
        ParentUiController.WorldController.LocalRepositoryController.UpdateUser(user);
        ParentUiController.WorldController.ServerRepositoryController.UpdateUser(user, new IResultListener<ZboomUser>()
        {
            OnError = () =>
            {
                UnityMainThreadDispatcher.Instance().Enqueue(() =>
                {
                    FinishSaveResult();
                    ResultOperationText.text = TEXT_ERROR;
                    ButtonRefresh.SetActive(true);
                });
            },

            OnSuccess = (savedUser) =>
            {
                UnityMainThreadDispatcher.Instance().Enqueue(() =>
                {
                    FinishSaveResult();
                    ResultOperationText.text = TEXT_SUCCESS;
                    ButtonRefresh.SetActive(false);
                });
            }
        });
    }

    private void StartSaveResult()
    {
        ResultOperationText.text = "���������� ��������...";
        ProgressPanel.SetActive(true);
        ButtonRefresh.SetActive(false);
        ButtonRestart.SetActive(false);
        ButtonBack.SetActive(false);
    }

    private void FinishSaveResult()
    {
        ProgressPanel.SetActive(false);
        ButtonRestart.SetActive(true);
        ButtonBack.SetActive(true);
    }

    public void RestartGame()
    {
        SceneManager.LoadScene("Menu");
    }

    public void RepeatSave()
    {
        SaveResultToServerDb(m_CurrentUser);
    }
}
