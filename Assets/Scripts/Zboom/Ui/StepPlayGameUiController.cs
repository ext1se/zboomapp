using Michsky.UI.ModernUIPack;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

[ExecuteInEditMode]
public class StepPlayGameUiController : BaseGameUiController, IScoreObserver
{
    [SerializeField]
    public GameObject ScoreObservable;
    public GameObject MainPanel;
    public Image IconScore;
    public TextMeshProUGUI TextScore;
    public bool IsShowName = true;
    public TextMeshProUGUI TextUsername;

    private IScoreObservable m_ScoreObservable;

    private void OnValidate()
    {
        ValidateHelper.Validate<IScoreObservable>(ScoreObservable);
    }

    protected override void Awake()
    {
        base.Awake();
        TextUsername.gameObject.SetActive(false);
        m_ScoreObservable = ScoreObservable.GetComponent<IScoreObservable>();
        m_ScoreObservable.AddObserver(this);
    }

    protected override void Activate()
    {
        MainPanel.SetActive(true);
    }

    protected override void Deactivate()
    {
        MainPanel.SetActive(false);
    }

    public void SetUsername(string name)
    {
        TextUsername.gameObject.SetActive(false);
        if (IsShowName)
        {
            if (name.Length > 3)
            {
                TextUsername.gameObject.SetActive(true);
                TextUsername.text = name;
            }
        }
    }

    public void UpdateScore(int score)
    {
        TextScore.text = "��� ����: " + score;
        TextScore.GetComponent<Animator>().SetTrigger("update");
        IconScore.GetComponent<Animator>().SetTrigger("update");
    }
}
