using Michsky.UI.ModernUIPack;
using UnityEngine;
using UnityEngine.UI;

public class StepScanAreaUiController : BaseGameUiController
{
    public ModalWindowManager WindowManager;
    public GameObject MainPanel;
    public GameObject NextButton;

    protected override void Start()
    {
        base.Start();
        NextButton.SetActive(false);
    }

    protected override void Activate()
    {
        MainPanel.SetActive(false);
        WindowManager.OpenWindow();
    }

    protected override void Deactivate()
    {
        MainPanel.SetActive(false);
        WindowManager.CloseWindow();
    }
}
