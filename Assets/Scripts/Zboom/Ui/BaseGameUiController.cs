using Michsky.UI.ModernUIPack;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BaseGameUiController : MonoBehaviour
{
    public GameUiController ParentUiController;

    [HideInInspector]
    public bool IsActive = false;

    protected virtual void Awake()
    {
        if (ParentUiController == null)
        {
            ParentUiController = GetComponentInParent<GameUiController>();
        }
    }

    protected virtual void Start()
    {

    }

    public void SetActive(bool isActive)
    {
        IsActive = isActive;
        gameObject.SetActive(IsActive);

        if (IsActive)
        {
            Activate();
        }
        else
        {
            Deactivate();
        }
    }

    protected abstract void Activate();

    protected abstract void Deactivate();
}
