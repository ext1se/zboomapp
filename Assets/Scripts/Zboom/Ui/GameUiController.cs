using Michsky.UI.ModernUIPack;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameUiController : MonoBehaviour
{
    public SlamGameWorldController WorldController;

    public BaseGameUiController StepScanAreaUiController;
    public BaseGameUiController StepSetRobotUiController;
    public BaseGameUiController StepStartGameUiController;
    public BaseGameUiController StepPlayGameUiController;
    public BaseGameUiController StepFinishGameUiController;

    public NotificationManager Notification;

    private void Start()
    {

    }

    private void Clear()
    {
        StepScanAreaUiController.SetActive(false);
        StepSetRobotUiController.SetActive(false);
        StepStartGameUiController.SetActive(false);
        StepPlayGameUiController.SetActive(false);
        StepFinishGameUiController.SetActive(false);
    }

    public void ShowNotification(string message = null)
    {
        if (message != null)
        {
            Notification.description = message;
            Notification.UpdateUI();
        }
        Notification.OpenNotification();
    }

    public void ScanArea()
    {
        Clear();
        StepScanAreaUiController.SetActive(true);
    }

    public void ScanArea(bool isSuccess)
    {
        if (StepScanAreaUiController is StepScanAreaUiController uiController)
        {
            uiController.NextButton.SetActive(isSuccess);
        }
    }

    public void SetRobotPosition()
    {
        Clear();
        StepSetRobotUiController.SetActive(true);
    }

    public void SetRobotPosition(bool isSuccess)
    {
        if (StepSetRobotUiController is StepSetRobotPositionUiController uiController)
        {
            uiController.NextButton.SetActive(isSuccess);
        }
    }

    public void StartGame()
    {
        Clear();
        StepStartGameUiController.SetActive(true);
    }

    public void PlayGame()
    {
        Clear();
        StepPlayGameUiController.SetActive(true);
        if (StepPlayGameUiController is StepPlayGameUiController uiController)
        {
            uiController.SetUsername(WorldController.CurrentUser.contact);
        }
    }

    public void FinishGame()
    {
        Clear();
        StepFinishGameUiController.SetActive(true);
        if (StepFinishGameUiController is StepFinishGameUiController uiController)
        {
            uiController.SetResult(WorldController.CurrentUser);
        }
    }
}
