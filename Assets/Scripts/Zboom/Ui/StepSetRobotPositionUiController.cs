using Michsky.UI.ModernUIPack;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StepSetRobotPositionUiController : BaseGameUiController
{
    public ModalWindowManager WindowManager;
    public GameObject MainPanel;
    public GameObject NextButton;

    protected override void Start()
    {
        base.Start();
        NextButton.SetActive(false);
    }

    protected override void Activate()
    {
        MainPanel.SetActive(false);
        WindowManager.OpenWindow();
    }

    protected override void Deactivate()
    {
        MainPanel.SetActive(false);
        WindowManager.CloseWindow();
    }
}
