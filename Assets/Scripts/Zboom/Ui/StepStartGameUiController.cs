using Michsky.UI.ModernUIPack;
using UnityEngine;
using UnityEngine.UI;

public class StepStartGameUiController : BaseGameUiController
{
    public ModalWindowManager WindowManager;
    public GameObject MainPanel;

    protected override void Start()
    {
        base.Start();
    }

    protected override void Activate()
    {
        MainPanel.SetActive(false);
        WindowManager.OpenWindow();
    }

    protected override void Deactivate()
    {
        MainPanel.SetActive(false);
        WindowManager.CloseWindow();
    }
}
