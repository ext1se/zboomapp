﻿using easyar;
using Firebase.Database;
using Firebase.Extensions;
using Michsky.UI.ModernUIPack;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Random = UnityEngine.Random;

[Serializable]
public class EasyArWorldController : SlamGameWorldController
{
    public ARSession Session;
    public TouchController TouchControl;

    private Color m_MeshColor;
    private VIOCameraDeviceUnion m_VioCamera;
    private DenseSpatialMapBuilderFrameFilter m_Dense;

    private bool m_CanMove = true;

    protected override void Awake()
    {
        base.Awake();
        m_VioCamera = Session.GetComponentInChildren<VIOCameraDeviceUnion>();
        m_Dense = Session.GetComponentInChildren<DenseSpatialMapBuilderFrameFilter>();
        TouchControl.TurnOn(TouchControl.gameObject.transform, Camera.main, false, false, false, false);
    }

    protected override void Start()
    {
        base.Start();
        m_MeshColor = m_Dense.MeshColor;
    }

    protected override void Update()
    {
        base.Update();
    }

    protected override void UpdateSetRobotPosition()
    {
        base.UpdateSetRobotPosition();
        if (CheckTouch())
        {
            Ray ray = m_CameraTarget.ScreenPointToRay(Input.touches[0].position);
            RaycastHit hitInfo;
            if (Physics.Raycast(ray, out hitInfo))
            {
                TouchControl.transform.position = hitInfo.point;
                Vector3 targetPostition = new Vector3(m_CameraTarget.transform.position.x,
                                    TouchControl.transform.position.y,
                                    m_CameraTarget.transform.position.z);
                TouchControl.transform.LookAt(targetPostition);
                GameUiController.SetRobotPosition(true);
            };
        }
    }

    public override void SetStateScanArea()
    {
        base.SetStateScanArea();
        StartCoroutine(UpdateVisibleArea());
    }

    private IEnumerator UpdateVisibleArea()
    {
        yield return new WaitForSeconds(1f);
        bool isVisible;
        if (m_Dense.MeshBlocks.Count > 5)
        {
            isVisible = true;
        }
        else
        {
            isVisible = false;
        }
        GameUiController.ScanArea(isVisible);
        StartCoroutine(UpdateVisibleArea());
    }

    public override void SetStateSetRobotPosition()
    {
        base.SetStateSetRobotPosition();
    }

    public override void SetStateStartGame()
    {
        base.SetStateStartGame();
        ShowMap(false);
        //TransparentMesh(true);
    }


    public override void SetStatePlayGame()
    {
        TouchControl.gameObject.SetActive(true);
        base.SetStatePlayGame();
    }

    public override void SetStateFinishGame()
    {
        TouchControl.gameObject.SetActive(false);
        base.SetStateFinishGame();
    }

    //------------------------------------------------//

    public void RotateModel(float value)
    {
        TouchControl.transform.rotation = Quaternion.Euler(
            TouchControl.transform.rotation.x,
            value,
            TouchControl.transform.rotation.z
        );
        //TouchControl.transform.RotateAround(Vector3.up, value);
    }

    public void ScaleModel(float value)
    {
        TouchControl.transform.localScale = new Vector3(value, value, value);
    }

    //------------------------------------------------//

    public void ShowMap(bool value)
    {
        if (!m_Dense)
        {
            return;
        }
        m_Dense.RenderMesh = value;
        m_Dense.UseCollider = value; //
    }

    public void ShowColor(bool value)
    {
        if (!m_Dense)
        {
            return;
        }
        m_Dense.RenderMesh = value;
        m_Dense.UseCollider = value; //
    }

    public void ShowMap()
    {
        ShowMap(!m_Dense.RenderMesh);
    }

    public void TransparentMesh(bool value)
    {
        if (!m_Dense)
        {
            return;
        }
        m_Dense.MeshColor = value ? Color.clear : m_MeshColor;
    }

    public void TransparentMesh()
    {
        if (m_Dense.MeshColor == Color.clear)
        {
            TransparentMesh(false);
        }
        else
        {
            TransparentMesh(true);
        }
    }

    public void EnableMove(bool move)
    {
        m_CanMove = move;
    }
}