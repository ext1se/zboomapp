using Doozy.Engine.SceneManagement;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartLogoController : MonoBehaviour
{
    public SceneLoader TransitionSceneLoader;

    public GameObject ServerRepositoryGameObject;
    public GameObject LocalRepositoryGameObject;

    [HideInInspector]
    public ZboomUser CurrentUser;
    public IRepository ServerRepositoryController;
    public IRepository LocalRepositoryController;

    protected virtual void OnValidate()
    {
        ValidateHelper.Validate<IRepository>(ServerRepositoryGameObject);
        ValidateHelper.Validate<IRepository>(LocalRepositoryGameObject);
    }

    protected void Start()
    {
        ServerRepositoryController = ServerRepositoryGameObject.GetComponent<IRepository>();
        LocalRepositoryController = LocalRepositoryGameObject.GetComponent<IRepository>();

        CurrentUser = LocalRepositoryController.GetUser();
        if (CurrentUser == null || !CurrentUser.IsAuthorizedUser())
        {
            StartCoroutine(LoadScene("Auth"));
        }
        else
        {
            StartCoroutine(LoadScene("Menu"));
        }
    }

    private IEnumerator LoadScene(string name)
    {
        yield return new WaitForSeconds(2.0f);
        //SceneManager.LoadScene(name);
        TransitionSceneLoader.LoadSceneAsyncSingle(name);
    }
}
