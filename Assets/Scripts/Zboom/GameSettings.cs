﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameSettings
{
    public const string KEY_SCORE = "KEY_SCORE";
    public const string KEY_USER = "KEY_USER";
    public const string KEY_CURRENT_USER = "KEY_CURRENT_USER";
    public const string KEY_USER_ID = "KEY_USER_ID";

    public GameSettings()
    {

    }

    public void SaveScore(int score)
    {
        PlayerPrefs.SetInt(KEY_SCORE, score);
    }

    public int GetScore()
    {
        return PlayerPrefs.GetInt(KEY_SCORE, 0);
    }

    public void SaveUser(string user)
    {
        PlayerPrefs.SetString(KEY_USER, user);
    }

    public string GetUser()
    {
        return PlayerPrefs.GetString(KEY_USER, "User");
    }

    public void SaveUserId(string userId)
    {
        PlayerPrefs.SetString(KEY_USER_ID, userId);
    }

    public ZboomUser GetCurrentUser()
    {
        string jsonUser = PlayerPrefs.GetString(KEY_CURRENT_USER, "");
        if (jsonUser.Length > 1)
        {
            return JsonUtility.FromJson<ZboomUser>(jsonUser);
        }
        else
        {
            return new ZboomUser();
        }
    }

    public void SaveCurrentUser(ZboomUser user)
    {
        string json = JsonUtility.ToJson(user);
        PlayerPrefs.SetString(KEY_CURRENT_USER, json);
    }

    public string GetUserId()
    {
        return PlayerPrefs.GetString(KEY_USER_ID, "UserId");
    }

    public void Delete()
    {
        PlayerPrefs.DeleteAll();
    }
}
