using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EmptyServerRepository : MonoBehaviour, IRepository
{
    private void Start()
    {

    }

    public void SaveUser(ZboomUser user, IResultListener<ZboomUser> resultListener)
    {
        resultListener.OnSuccess(user);
    }

    public void UpdateUser(ZboomUser user, IResultListener<ZboomUser> resultListener)
    {
        resultListener.OnSuccess(user);
    }

    public ZboomUser GetUser(string id, IResultListener<ZboomUser> resultListener)
    {
        resultListener.OnSuccess(null);
        return null;
    }

    public void SaveScore(ZboomUser user, int score, IResultListener<ZboomUser> resultListener)
    {
        user.score = score;
        resultListener.OnSuccess(user);
    }
}

