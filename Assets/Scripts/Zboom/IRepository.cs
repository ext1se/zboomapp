using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IRepository
{
    public void SaveUser(ZboomUser user, IResultListener<ZboomUser> resultListener = null);

    public void UpdateUser(ZboomUser user, IResultListener<ZboomUser> resultListener = null);

    public ZboomUser GetUser(string id = null, IResultListener<ZboomUser> resultListener = null);

    public void SaveScore(ZboomUser user, int score, IResultListener<ZboomUser> resultListener = null);

}

public class IResultListener<T>
{
    public Action<T> OnSuccess;

    public Action OnError;

    public Action OnFinish;
}
