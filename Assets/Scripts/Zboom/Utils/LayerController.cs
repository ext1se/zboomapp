using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LayerController : MonoBehaviour
{
    public const string LAYER_DEFAULT = "Default"; 
    public const string LAYER_AR_UI = "AR UI";

    public bool IsAlwaysVisible = false;

    void Start()
    {
        SetVisible(IsAlwaysVisible, true);
    }

    public void SetVisible(bool isVisible, bool changeChild = true)
    {
        string layer = isVisible ? LAYER_AR_UI : LAYER_DEFAULT;
        IsAlwaysVisible = isVisible;
        gameObject.layer = LayerMask.NameToLayer(layer);
        if (changeChild)
        {
            foreach (Transform child in transform)
            {
                child.gameObject.layer = LayerMask.NameToLayer(layer);
            }
        }
    }
}
