﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class ValidateHelper
{
    public static void Validate<T>(GameObject gameobject)
    {
        if (gameobject.GetComponent<T>() == null)
        {
            gameobject = null;
        }
    }
}
