﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine.UI;
using System.Threading.Tasks;
using UnityEngine;
using System.Collections;

public class SlamGameWorldController : GameWorldController
{
    public enum StepType
    {
        SCAN_AREA,
        SET_ROBOT_POSITION,
        START_GAME,
        PLAY_GAME,
        FINISH_GAME,
        RESTART_GAME,
        UPDATE_GAME
    }

    public StepType CurrentGameStep = StepType.SCAN_AREA;
    public HeroController HeroController;
    public GameUiController GameUiController;
    public GameUiArController GameUiArController;
    public TokenGeneratorController TokenGeneratorController;

    [SerializeField]
    protected float m_MaxDistance = 2.5f;

    protected override void Awake()
    {
        base.Awake();
    }

    protected override void Start()
    {
        base.Start();
        switch (CurrentGameStep)
        {
            case StepType.SCAN_AREA:
                SetStateScanArea();
                break;
            case StepType.SET_ROBOT_POSITION:
                SetStateSetRobotPosition();
                break;
            case StepType.START_GAME:
                SetStateStartGame();
                break;
            case StepType.PLAY_GAME:
                SetStatePlayGame();
                break;
            case StepType.FINISH_GAME:
                SetStateFinishGame();
                break;
            case StepType.RESTART_GAME:
                SetStateRestartGame();
                break;
            case StepType.UPDATE_GAME:
                SetStateUpdateGame();
                break;
        }
    }

    protected virtual void Update()
    {
        base.Update();
        switch (CurrentGameStep)
        {
            case StepType.SCAN_AREA:
                UpdateScanArea();
                break;
            case StepType.SET_ROBOT_POSITION:
                UpdateSetRobotPosition();
                break;
            case StepType.START_GAME:
                UpdateStartGame();
                break;
            case StepType.PLAY_GAME:
                UpdatePlayGame();
                break;
            case StepType.FINISH_GAME:
                UpdateFinishGame();
                break;
            case StepType.RESTART_GAME:
                UpdateRestartGame();
                break;
            case StepType.UPDATE_GAME:
                break;
        }
    }

    protected virtual void UpdateScanArea()
    {

    }

    protected virtual void UpdateSetRobotPosition()
    {

    }

    protected virtual void UpdateStartGame()
    {

    }

    protected virtual void UpdatePlayGame()
    {
        if (CheckTouch())
        {
            Ray ray = m_CameraTarget.ScreenPointToRay(Input.touches[0].position);
            RaycastHit hitInfo;
            if (Physics.Raycast(ray, out hitInfo))
            {
                if (hitInfo.transform.GetComponent<TokenController>())
                {
                    TokenController tokenController = hitInfo.transform.GetComponent<TokenController>();
                    float dist = Vector3.Distance(Camera.main.transform.position, tokenController.transform.position);
                    if (dist < m_MaxDistance)
                    {
                        if (tokenController.IsEnabled)
                        {
                            IncreaseScore();
                            tokenController.Hide();
                            TokenGeneratorController.GenerateToken();
                        }
                    }
                    else
                    {
                        GameUiController.ShowNotification();
                        tokenController.Show();
                    }
                }
            };
        }
    }

    protected virtual void UpdateFinishGame()
    {

    }

    protected virtual void UpdateRestartGame()
    {

    }

    public virtual void SetStateScanArea()
    {
        StopAllCoroutines();
        CurrentGameStep = StepType.SCAN_AREA;
        GameUiController.ScanArea();
    }

    public virtual void SetStateSetRobotPosition()
    {
        StopAllCoroutines();
        CurrentGameStep = StepType.SET_ROBOT_POSITION;
        GameUiController.SetRobotPosition();
    }

    public virtual void SetStateStartGame()
    {
        StopAllCoroutines();
        CurrentGameStep = StepType.START_GAME;
        GameUiController.StartGame();
        //GameUiArController.StartGame();
    }

    public virtual void SetStatePlayGame()
    {
        StopAllCoroutines();
        CurrentGameStep = StepType.PLAY_GAME;
        TokenGeneratorController.GenerateTokens();
        GameUiController.PlayGame();
        GameUiArController.PlayGame();
        //m_Score = CurrentUser.score;
        UpdateScore();
    }

    public virtual void SetStateRestartGame()
    {
        StopAllCoroutines();
        CurrentGameStep = StepType.RESTART_GAME;
    }

    public virtual void SetStateFinishGame()
    {
        TokenGeneratorController.ClearTokens(true);
        StopAllCoroutines();
        CurrentGameStep = StepType.FINISH_GAME;
        GameUiController.FinishGame();
    }

    public virtual void SetStateUpdateGame()
    {
        StopAllCoroutines();
        CurrentGameStep = StepType.UPDATE_GAME;
    }

    protected override void UpdateScore()
    {
        base.UpdateScore();
        if (m_Score != 0)
        {
            if (HeroController.gameObject.activeSelf)
            {
                HeroController.GetToken();
            }
        }
    }
}
