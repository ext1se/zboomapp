using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TokenGeneratorController : MonoBehaviour
{
    public TokenController CoinPrefab;
    public int CountToken = 5;
    public float Delay = 0;

    [SerializeField]
    private float m_MinDistance = 1f;
    [SerializeField]
    private float m_MaxDistance = 5f;
    private bool m_IsFirstToken = true;

    public void GenerateTokens()
    {
        ClearTokens();

        for (int i = 0; i < CountToken; i++)
        {
            GenerateToken();
        }
    }

    public void ClearTokens(bool useDestroy = false)
    {
        TokenController[] tokenControllers = GetComponentsInChildren<TokenController>();
        foreach (TokenController tokenController in tokenControllers)
        {
            if (!useDestroy)
            {
                tokenController.Hide(false);
            }
            else
            {
                Destroy(tokenController.gameObject);
            }
        }
    }

    public void GenerateToken()
    {
        StartCoroutine(GenerateTokenAsync());
    }

    private IEnumerator GenerateTokenAsync()
    {
        yield return new WaitForSeconds(Delay);
        float baseY = 1f;
        float x = Random.Range(m_MinDistance, m_MaxDistance);
        if (Random.value <= 0.5f)
        {
            x *= -1;
        }
        float z = Random.Range(m_MinDistance, m_MaxDistance);
        if (Random.value <= 0.5f)
        {
            z *= -1;
        }
        float y = baseY / 2 + Random.value / 1.5f;
        Vector3 position = new Vector3(x, y, z);

        if (m_IsFirstToken)
        {
            position = new Vector3(0, 1f, 1f);
            m_IsFirstToken = false;
        }

        TokenController coinController = Instantiate(CoinPrefab, transform) as TokenController;
        coinController.transform.localPosition = position;
    }

    public void MagnetizeTokens()
    {
        TokenController[] tokenControllers = GetComponentsInChildren<TokenController>();
        foreach (TokenController tokenController in tokenControllers)
        {
            tokenController.Magnetize();
        }
    }
}
