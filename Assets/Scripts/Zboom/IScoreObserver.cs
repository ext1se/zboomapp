using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IScoreObserver
{
    public void UpdateScore(int score);
}
