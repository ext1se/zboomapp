﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchCoinController : MonoBehaviour
{
    public bool IsAdminMode = false;
    void Update()
    {
        if (IsAdminMode)
        {
            return;
        }
        if ((Input.touchCount > 0 && Input.touches[0].phase == TouchPhase.Ended) || Input.GetMouseButtonUp(0))
        {
            RaycastHit hit;
            Ray ray;
            if (Input.touchCount > 0)
            {
                ray = Camera.main.ScreenPointToRay(Input.touches[0].position);
            }
            else
            {
                ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            }
            if (Physics.Raycast(ray, out hit))
            {
                if (hit.transform != null)
                {
                    if (hit.transform.GetComponent<TokenController>())
                    {
                        //hit.transform.GetComponent<CoinController>().Open();
                        return;
                    }
                }
            }
        }
    }
}
