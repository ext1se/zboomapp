﻿using Firebase.Database;
using Firebase.Extensions;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UiResultController : MonoBehaviour
{
    public TextMeshProUGUI ResultText;
    public TextMeshProUGUI ResultOperationText;
    public TextMeshProUGUI Name;
    public GameObject ProgressPanel;
    public GameObject ButtonRefresh;
    public GameObject ButtonRestart;
    public GameObject ButtonBack;

    private ZboomUser currentUser;

    private void Start()
    {

    }

    public void SetResult(ZboomUser user)
    {
        currentUser = user;
        ResultText.text = "" + user.score;

        ProgressPanel.SetActive(false);
        ButtonRefresh.SetActive(false);
        ButtonRestart.SetActive(true);
        ButtonBack.SetActive(true);

        if (user.contact.Length > 2)
        {
            Name.gameObject.SetActive(true);
            Name.text = user.contact;

            user.date_updated = DateTime.Now.ToString("MM/dd/yyyy h:mm");
            SaveResultToDb(user);
        }
        else
        {
            Name.gameObject.SetActive(false);
        }
        //StartCoroutine(SaveResult());
    }

    private void SaveResultToDb(ZboomUser user)
    {
        ResultOperationText.text = "Сохранение результа...";
        ProgressPanel.SetActive(true);
        ButtonRefresh.SetActive(false);
        ButtonRestart.SetActive(false);
        ButtonBack.SetActive(false);
        FirebaseDatabase.DefaultInstance.RootReference.Child("users").Child(user.id).SetRawJsonValueAsync(JsonUtility.ToJson(user)).ContinueWithOnMainThread(task =>
        {
            ProgressPanel.SetActive(false);
            ButtonRestart.SetActive(true);
            ButtonBack.SetActive(true);
            if (task.IsCompleted)
            {
                ResultOperationText.text = "Результат успешно сохранен и загружен!";
                ButtonRefresh.SetActive(false);
            }
            else
            {
                ResultOperationText.text = "Произошла ошибка, попробуйте снова.";
                ButtonRefresh.SetActive(true);
            }
        });
    }

    private IEnumerator SaveResult()
    {
        ResultOperationText.text = "Сохранение результа...";
        ProgressPanel.SetActive(true);
        ButtonRefresh.SetActive(false);
        ButtonRestart.SetActive(false);
        yield return new WaitForSeconds(2f);
        if (UnityEngine.Random.value > 0.75f)
        {
            ResultOperationText.text = "Результат успешно сохранен и загружен!";
            ButtonRefresh.SetActive(false);
            ButtonRestart.SetActive(true);
        }
        else
        {
            ResultOperationText.text = "Произошла ошибка, попробуйте снова.";
            ButtonRefresh.SetActive(true);
            ButtonRestart.SetActive(false);
        }
        ProgressPanel.SetActive(false);
    }

    public void RestartGame()
    {
        //SceneManager.LoadScene("Izx");
        SceneManager.LoadScene(0);
    }

    public void Repeat()
    {
        //StartCoroutine(SaveResult());
        SaveResultToDb(currentUser);
    }
}
