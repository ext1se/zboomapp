﻿using Firebase.Database;
using Firebase.Extensions;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;

public class AuthWorldControllerOld : MonoBehaviour
{
    public TMP_InputField ContactText;
    public TextMeshProUGUI Info;
    public GameObject ButtonNext;
    public GameObject ProgressBar;
    public GameObject ServerRepositoryController;
    public GameObject LocalRepositoryController;

    private bool m_IsSuccessFirebase = true;
    //private GameSettings m_GameSetting;
    private IRepository m_ServerRepositoryController;
    private IRepository m_LocalRepositoryController;

    private void OnValidate()
    {
        if (ServerRepositoryController != null)
        {
            m_ServerRepositoryController = ServerRepositoryController.GetComponent<IRepository>();
            if (m_ServerRepositoryController == null)
            {
                ServerRepositoryController = null;
            }
        }

        if (LocalRepositoryController != null)
        {
            m_LocalRepositoryController = LocalRepositoryController.GetComponent<IRepository>();
            if (m_LocalRepositoryController == null)
            {
                LocalRepositoryController = null;
            }
        }
    }

    private void Start()
    {
        ZboomUser currentUser = m_LocalRepositoryController.GetUser();
        ContactText.text = currentUser.contact;

        if (m_ServerRepositoryController is FirebaseController serverRepositoryController)
        {
            serverRepositoryController.Init();
        }
    }

    public void LoadNextScene()
    {
        //ButtonNext.SetActive(false);
        ProgressBar.SetActive(true);

        ZboomUser user = new ZboomUser();
        DatabaseReference reference = FirebaseDatabase.DefaultInstance.RootReference;
        string contact = ContactText.text.Trim().ToLower();
        user.contact = contact;
        if (contact.Length > 1)
        {
            IResultListener<ZboomUser> resultListener = new IResultListener<ZboomUser>();
            m_ServerRepositoryController.GetUser(contact, new IResultListener<ZboomUser>()
            {
                OnSuccess = (user) =>
                {
                    if (user == null)
                    {
                        m_ServerRepositoryController.SaveUser(user, new IResultListener<ZboomUser>()
                        {
                            OnSuccess = (savedUser) =>
                            {
                                //m_GameSetting.SaveCurrentUser(savedUser);
                                SceneManager.LoadScene("Izx");
                            },
                            OnError = () =>
                            {
                                CreateEmptyUser(contact);
                            }
                        });
                    }
                    else
                    {
                        //m_GameSetting.SaveCurrentUser(user);
                        SceneManager.LoadScene("Izx");
                    }
                },
                OnError = () =>
                {
                    CreateEmptyUser(contact);
                }
            }
            );
        }
        else
        {
            CreateEmptyUser(contact);
        }
    }

    private void CreateEmptyUser(string contact)
    {
        ZboomUser newUser = new ZboomUser();
        newUser.id = "0";
        newUser.contact = contact;
        //m_GameSetting.SaveCurrentUser(newUser);
        SceneManager.LoadScene("Izx");
    }

    public void OpenDb()
    {
        //DatabaseReference reference = FirebaseDatabase.DefaultInstance.RootReference;
        //writeNewUser();
    }


    TransactionResult AddScoreTransaction(MutableData mutableData)
    {
        int id = Random.Range(0, 10);

        ZboomUser user = mutableData.Value as ZboomUser;
        string idEmail = "_test_user_" + id + "@mail.com";
        user.contact = idEmail;
        user.id = "Test User" + id;
        user.id = "id_test_user_" + id;
        user.score = Random.Range(0, 100) * (-1);
        string json = JsonUtility.ToJson(user);
        mutableData.Value = user;
        return TransactionResult.Success(mutableData);
    }

    public void WriteNewScore()
    {
        // Create new entry at /user-scores/$userid/$scoreid and at
        // /leaderboard/$scoreid simultaneously
        DatabaseReference reference = FirebaseDatabase.DefaultInstance.RootReference;
        string key = reference.Child("scores").Push().Key;
    }

    public void GetData()
    {
        DatabaseReference reference = FirebaseDatabase.DefaultInstance.GetReference("users");
        reference
            .GetValueAsync().ContinueWith(task =>
            {
                if (task.IsFaulted)
                {
                    // Handle the error...
                }
                else if (task.IsCompleted)
                {
                    DataSnapshot snapshot = task.Result;
                    // Do something with snapshot...
                }
            });
    }
}
