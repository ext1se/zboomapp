using Firebase.Database;
using Firebase.Extensions;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public class FirebaseController : MonoBehaviour, IRepository
{
    //[SerializeField]
    //private string m_ReferenceName = "users_test";
    private string m_ReferenceName = "users"; 
    private bool m_IsSuccessFirebase = true;

    private void Start()
    {

    }

    public void Init()
    {
        Firebase.FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task =>
        {
            var dependencyStatus = task.Result;
            if (dependencyStatus == Firebase.DependencyStatus.Available)
            {
                m_IsSuccessFirebase = true;
            }
            else
            {
                m_IsSuccessFirebase = false;
                Debug.LogError(System.String.Format("Could not resolve all Firebase dependencies: {0}",
                    dependencyStatus));
            }
        });
    }

    public void SaveUser(ZboomUser user, IResultListener<ZboomUser> resultListener)
    {
        if (!m_IsSuccessFirebase)
        {
            resultListener.OnError();
            return;
        }

        string key = FirebaseDatabase.DefaultInstance
            .GetReference(m_ReferenceName)
            .Push()
            .Key;
        user.id = key;
        string date = DateTime.Now.ToString("MM/dd/yyyy h:mm");
        user.date_registrated = date;
        user.date_updated = date;
        string json = JsonUtility.ToJson(user);
        FirebaseDatabase.DefaultInstance
            .GetReference(m_ReferenceName)
            .Child(key)
            .SetRawJsonValueAsync(json)
            .ContinueWith(task =>
        {
            // Thread.Sleep(10000); // For test async
            if (task.IsFaulted)
            {
                resultListener.OnError();
            }
            else
            {
                if (task.IsCompleted)
                {
                    resultListener.OnSuccess(user);
                }
                else
                {
                    resultListener.OnError();
                }
            }
        });
    }

    public void UpdateUser(ZboomUser user, IResultListener<ZboomUser> resultListener)
    {
        if (!m_IsSuccessFirebase)
        {
            resultListener.OnError();
            return;
        }

        string json = JsonUtility.ToJson(user);
        FirebaseDatabase.DefaultInstance
           .GetReference(m_ReferenceName)
           .Child(user.id)
           .SetRawJsonValueAsync(json)
           .ContinueWith(task =>
           {
               if (task.IsFaulted)
               {
                   resultListener.OnError();
               }
               else
               {
                   if (task.IsCompleted)
                   {
                       resultListener.OnSuccess(user);
                   }
                   else
                   {
                       resultListener.OnError();
                   }
               }
           });
    }

    public ZboomUser GetUser(string id, IResultListener<ZboomUser> resultListener)
    {
        if (!m_IsSuccessFirebase)
        {
            resultListener.OnError();
            return null;
        }

        string contact = id;
        if (id != null && id.Length > 0)
        {
            FirebaseDatabase.DefaultInstance
                        .GetReference(m_ReferenceName)
                        .OrderByChild("contact")
                        .EqualTo(contact)
                        .LimitToFirst(1)
                        .GetValueAsync()
                        .ContinueWith(task =>
                        {
                            if (task.IsFaulted)
                            {
                                resultListener.OnError();
                            }
                            else
                            {
                                if (task.IsCompleted)
                                {
                                    DataSnapshot snapshot = task.Result;
                                    if (snapshot.ChildrenCount > 0)
                                    {
                                        var childs = snapshot.Children.GetEnumerator();
                                        childs.MoveNext();
                                        string json = childs.Current.GetRawJsonValue();
                                        ZboomUser user = JsonUtility.FromJson<ZboomUser>(json);
                                        resultListener.OnSuccess(user);
                                    }
                                    else
                                    {
                                        resultListener.OnSuccess(null);
                                    }
                                }
                                else
                                {
                                    resultListener.OnError();
                                }
                            }

                        });
        }
        return null;
    }

    public void SaveScore(ZboomUser user, int score, IResultListener<ZboomUser> resultListener)
    {
        if (!m_IsSuccessFirebase)
        {
            resultListener.OnError();
            return;
        }

        user.score = score;
        string json = JsonUtility.ToJson(user);
        FirebaseDatabase.DefaultInstance
           .GetReference(m_ReferenceName)
           .Child(user.id)
           .SetRawJsonValueAsync(json)
           .ContinueWith(task =>
           {
               if (task.IsFaulted)
               {
                   resultListener.OnError();
               }
               else
               {
                   if (task.IsCompleted)
                   {
                       resultListener.OnSuccess(user);
                   }
                   else
                   {
                       resultListener.OnError();
                   }
               }
           });
    }
}
