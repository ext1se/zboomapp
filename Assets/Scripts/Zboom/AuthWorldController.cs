﻿using Doozy.Engine.SceneManagement;
using Firebase.Database;
using Firebase.Extensions;
using Michsky.UI.ModernUIPack;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;

public class AuthWorldController : MonoBehaviour
{
    public TMP_InputField ContactText;
    public TextMeshProUGUI Info;
    public GameObject ButtonNext;
    public GameObject ButtonSkip;
    public GameObject ProgressBar;
    //public HorizontalSelector HorizontalSelector;
    public GameObject ServerRepositoryController;
    public GameObject LocalRepositoryController;

    public SceneLoader TransitionSceneLoader;
    [SerializeField]
    private string m_SceneName = "RobotSlamAr";
    private IRepository m_ServerRepositoryController;
    private IRepository m_LocalRepositoryController;

    private void OnValidate()
    {
        ValidateHelper.Validate<IRepository>(ServerRepositoryController);
        ValidateHelper.Validate<IRepository>(LocalRepositoryController);
    }

    private void Start()
    {
        m_ServerRepositoryController = ServerRepositoryController.GetComponent<IRepository>();
        m_LocalRepositoryController = LocalRepositoryController.GetComponent<IRepository>();

        ZboomUser currentUser = m_LocalRepositoryController.GetUser();
        ContactText.text = currentUser.contact;

        if (m_ServerRepositoryController is FirebaseController serverRepositoryController)
        {
            serverRepositoryController.Init();
        }
        /*
        HorizontalSelector.index = 0;
        HorizontalSelector.defaultIndex = 0;
        HorizontalSelector.UpdateUI();
        */
    }

    public void Login()
    {
        //HorizontalSelector.gameObject.SetActive(false);
        ButtonNext.SetActive(false);
        ButtonSkip.SetActive(false);
        ProgressBar.SetActive(true);
        ZboomUser user = new ZboomUser();
        string contact = ContactText.text.Trim().ToLower();
        user.id = "-1";
        user.contact = contact;
        if (contact.Length > 1)
        {
            m_ServerRepositoryController.GetUser(contact, new IResultListener<ZboomUser>()
            {
                OnSuccess = (authUser) =>
                {
                    if (authUser == null)
                    {
                        m_ServerRepositoryController.SaveUser(user, new IResultListener<ZboomUser>()
                        {
                            OnSuccess = (savedUser) =>
                            {
                                LoadNextScene(savedUser);
                            },
                            OnError = () =>
                            {
                                LoadNextScene(user);
                            }
                        });
                    }
                    else
                    {
                        LoadNextScene(authUser);
                    }
                },
                OnError = () =>
                {
                    LoadNextScene(user);
                }
            }
            );
        }
        else
        {
            LoadNextScene(user);
        }
    }

    private void LoadNextScene(ZboomUser user)
    {
        UnityMainThreadDispatcher.Instance().Enqueue(() =>
        {
            m_LocalRepositoryController.SaveUser(user);
            /*
            int pos = HorizontalSelector.index;
            switch (pos)
            {
                case (2):
                    m_SceneName = "EasyArRobotSlam";
                    break;
                case (0):
                    m_SceneName = "EasyArPurpleWizardSlam";
                    break;
                case (1):
                    m_SceneName = "EasyArRedWizardSlam";
                    break;
                default:
                    m_SceneName = "EasyArRobotSlam";
                    break;
            }
            */
            TransitionSceneLoader.LoadSceneAsyncSingle(m_SceneName);
            //SceneManager.LoadScene(m_SceneName);
        });
    }
}
