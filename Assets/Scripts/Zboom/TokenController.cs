﻿using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using TMPro;
using UnityEngine;

public class TokenController : MonoBehaviour
{
    [HideInInspector]
    public bool IsEnabled = true;
    public bool ShowDistance = true;
    public TextMeshProUGUI DistanceText;

    private Transform m_Target;
    private Animator m_Animator;
    private AudioSource m_AudioSource;

    private bool m_IsMagnited = false;
    private float m_Speed = 1.0f;

    private void Start()
    {
        m_Target = Camera.main.transform;
        m_Animator = GetComponent<Animator>();
        m_AudioSource = GetComponent<AudioSource>();
        Show(false);

        if (ShowDistance)
        {
            //DistanceText.gameObject.SetActive(true);
            StartCoroutine(UpdateDistance());
        }
        else
        {
            DistanceText.gameObject.SetActive(false);
        }
    }

    private IEnumerator UpdateDistance()
    {
        yield return new WaitForSeconds(0.2f);
        float dist = Vector3.Distance(m_Target.position, transform.position);
        if (dist < 2.0f)
        {
            DistanceText.gameObject.SetActive(false);
        }
        else
        {
            DistanceText.text = dist.ToString("N", CultureInfo.CreateSpecificCulture("ru-RU")) + " м.";
            DistanceText.gameObject.SetActive(true);
        }
        StartCoroutine(UpdateDistance());
    }

    private void Update()
    {
        if (m_IsMagnited)
        {
            if (Vector3.Distance(transform.position, m_Target.transform.position) < 1.2f)
            {
                return;
            }
            else
            {
                float step = m_Speed * Time.deltaTime; // calculate distance to move
                transform.position = Vector3.MoveTowards(transform.position, m_Target.transform.position, step);
            }
        }
    }

    public void Magnetize()
    {
        StartCoroutine(StartMagnetize());
    }

    private IEnumerator StartMagnetize()
    {
        m_IsMagnited = true;
        yield return new WaitForSeconds(5f);
        m_IsMagnited = false;
    }

    public void Show(bool isPlaySound = true)
    {
        IsEnabled = true;
        m_Animator.SetTrigger("show");
        if (isPlaySound)
        {
            m_AudioSource.Play();
        }
    }

    public void Hide(bool isPlaySound =  true)
    {
        IsEnabled = false;
        m_Animator.SetTrigger("hide");
        StopAllCoroutines();
        Destroy(gameObject, 1f);
        if (isPlaySound)
        {
            m_AudioSource.Play();
        }
    }
}
