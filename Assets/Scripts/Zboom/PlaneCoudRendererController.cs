﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;

public class PlaneCoudRendererController : MonoBehaviour
{
    [SerializeField]
    [Tooltip("Disables spawned feature points and the ARPointCloudManager")]
    bool m_EnableFeaturePoints;

    public bool EnableFeaturePoints
    {
        get => m_EnableFeaturePoints;
        set
        {
            m_EnableFeaturePoints = value;
            m_PointCloudManager.SetTrackablesActive(m_EnableFeaturePoints);
            m_PointCloudManager.enabled = m_EnableFeaturePoints;
        }
    }

    [SerializeField]
    [Tooltip("Disables spawned planes and ARPlaneManager")]
    bool m_EnablePlaneRendering;

    public bool EnablePlaneRendering
    {
        get => m_EnablePlaneRendering;
        set
        {
            m_EnablePlaneRendering = value;
            m_PlaneManager.SetTrackablesActive(m_EnablePlaneRendering);
            m_PlaneManager.enabled = m_EnablePlaneRendering;
        }
    }

    [SerializeField]
    private ARPointCloudManager m_PointCloudManager;

    public ARPointCloudManager PointCloudManager
    {
        get => m_PointCloudManager;
        set => m_PointCloudManager = value;
    }

    [SerializeField]
    private ARPlaneManager m_PlaneManager;

    public ARPlaneManager PlaneManager
    {
        get => m_PlaneManager;
        set => m_PlaneManager = value;
    }

    protected void Update()
    {
        if (m_PlaneManager.trackables.count > 2)
        {
            EnablePlaneRendering = false;
        }
    }
}
