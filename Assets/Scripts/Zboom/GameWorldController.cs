﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.EventSystems;

public abstract class GameWorldController : MonoBehaviour, IScoreObservable
{
    public bool IsNewGame = false;
    public GameObject ServerRepositoryGameObject;
    public GameObject LocalRepositoryGameObject;

    [HideInInspector]
    public ZboomUser CurrentUser;
    public IRepository ServerRepositoryController;
    public IRepository LocalRepositoryController;

    protected List<IScoreObserver> m_ScoreObservers;
    protected Camera m_CameraTarget;
    protected int m_Score = 0;

    protected virtual void OnValidate()
    {
        ValidateHelper.Validate<IRepository>(ServerRepositoryGameObject);
        ValidateHelper.Validate<IRepository>(LocalRepositoryGameObject);
    }

    protected bool CheckTouch()
    {
        if (Input.touchCount > 0) // == 1
        {
            if (!EventSystem.current.IsPointerOverGameObject(Input.GetTouch(0).fingerId)
                && Input.touches[0].phase == TouchPhase.Began)
            {
                return true;
            }
        }
        return false;
    }

    protected virtual void Awake()
    {
        ServerRepositoryController = ServerRepositoryGameObject.GetComponent<IRepository>();
        LocalRepositoryController = LocalRepositoryGameObject.GetComponent<IRepository>();

        CurrentUser = LocalRepositoryController.GetUser();
        m_Score = CurrentUser.score;
        if (IsNewGame)
        {
            m_Score = 0;
        }
        m_ScoreObservers = new List<IScoreObserver>();
        m_CameraTarget = Camera.main;
    }

    protected virtual void Start()
    {

    }

    protected virtual void Update()
    {

    }

    protected virtual void UpdateScore()
    {
        NotifyObservers();
    }

    protected virtual void IncreaseScore()
    {
        m_Score++;

        UpdateScore();

        CurrentUser.score = m_Score;
        CurrentUser.date_updated = DateTime.Now.ToString("MM/dd/yyyy h:mm");
        // if (CurrentUser.IsAuthorizedUser()){}
        LocalRepositoryController.SaveUser(CurrentUser);
        if (m_Score % 5 == 0 && CurrentUser.IsAuthorizedUser())
        {
            ServerRepositoryController.UpdateUser(CurrentUser, new IResultListener<ZboomUser>());
        }
    }

    public void AddObserver(IScoreObserver scoreObserver)
    {
        m_ScoreObservers.Add(scoreObserver);
    }

    public void RemoveObserver(IScoreObserver scoreObserver)
    {
        m_ScoreObservers.Remove(scoreObserver);
    }

    public void NotifyObservers()
    {
        m_ScoreObservers.ForEach(scoreObserver => scoreObserver.UpdateScore(m_Score));
    }
}
