﻿using easyar;
using Michsky.UI.ModernUIPack;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EasyArBonusController : BonusController
{
    public ImageTargetController ImageTargetController;

    protected override void Start()
    {
        base.Start();
        AddTargetControllerEvents();
    }

    private void AddTargetControllerEvents()
    {
        if (!ImageTargetController)
        {
            return;
        }

        ImageTargetController.TargetFound += () =>
        {
            Show();
        };
        ImageTargetController.TargetLost += () =>
        {
            Hide();
        };
        ImageTargetController.TargetLoad += (Target target, bool status) =>
        {
            Show();
        };
        ImageTargetController.TargetUnload += (Target target, bool status) =>
        {
            Hide();
        };
    }
}
