﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WizardController : HeroController
{
    public GameObject MagicSpellPrefab;

    private AudioSource m_AudioSource;
    private Animator m_Animator;
    [SerializeField]
    private float m_CastDelay = 0.8f;

    private void Start()
    {
        m_Animator = GetComponent<Animator>();
        m_AudioSource = GetComponent<AudioSource>();
    }

    public void Talk()
    {
        m_Animator.SetTrigger("talking");
        m_AudioSource.Play();
    }

    public override void GetToken()
    {
        m_Animator.SetTrigger("magic");
        StopAllCoroutines();
        StartCoroutine(CastSpell());
    }

    private IEnumerator CastSpell()
    {
        yield return new WaitForSeconds(m_CastDelay);
        GameObject spell = Instantiate(MagicSpellPrefab, transform);
        Destroy(spell, 3f);
    }
}
