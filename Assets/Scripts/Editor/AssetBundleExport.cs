﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class AssetBundleExport : Editor
{
    [MenuItem("Assets/Build AssetBundles")]
    static void BuildAllAssetBundles()
    {
        BuildPipeline.BuildAssetBundles("Assets/AssetBundles~/Android",
            //BuildAssetBundleOptions.ChunkBasedCompression,
            BuildAssetBundleOptions.None,
            BuildTarget.Android);


        /*
        //BuildPipeline.BuildAssetBundles(@"J:\Unity\Models\Web",
        BuildPipeline.BuildAssetBundles("Assets/AssetBundles~/Web",
            BuildAssetBundleOptions.None,
            BuildTarget.WebGL);
            */

        /*
        BuildPipeline.BuildAssetBundles(@"J:\Unity\Models\iOS",
            BuildAssetBundleOptions.None,
            BuildTarget.iOS);
            */
    }
}