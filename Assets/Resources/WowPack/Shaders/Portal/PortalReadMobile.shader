﻿Shader "Portal/ReadMobile" {
	Properties {
		_MainTex ("Texture", 2D) = "white" {}
		[IntRange] _StencilRef ("Stencil Reference Value", Range(0,255)) = 0
	}
	SubShader {
		Tags{ "RenderType"="Opaque" "Queue"="Geometry"}

		Stencil{
			Ref [_StencilRef]
			Comp Equal
		}

		Pass {
			Lighting Off
			SetTexture[_MainTex]
		}
	}

	FallBack "Unlit/Texture"
}